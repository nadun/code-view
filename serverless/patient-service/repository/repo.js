const dynamoose = require('./connector-dynamodb');
const schema = require('./schema');

const repository = dynamoose.model('Patient', schema);

module.exports = repository;
