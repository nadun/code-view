const dynamoose = require('./connector-dynamodb');

const { Schema } = dynamoose;

var Patient = new Schema({
    PatientId: {
        type: String,
        hashKey: true
    },
    Name: {
        type: String
    },
    Email: {
        type: String
    },
    PhoneNumber: {
        type: String
    },
    CaseDate: {
        type: String
    },
    AppointmentDate: {
        type: String
    },
    Age: {
        type: Number
    },
    Gender: {
        type: String
    },
    Birthday: {
        type: String
    },
    Address: {
        type: String
    },
    Comments: {
        type: String
    },
    CreatedBy: {
        type: String,
        index: {
            global: true,
            name: 'User-Index',
            throughput: {
                read: 2,
                write: 2
            } // read and write are both 1
        }
    },
    CreatedByEmail: {
        type: String,
        index: {
            global: true,
            name: 'Email-Index',
            throughput: {
                read: 2,
                write: 2
            } // read and write are both 1
        }
    }
}, {
    useDocumentTypes: true,
    throughput: {
        read: 2,
        write: 2
    },
    timestamps: {
        createdAt: 'CreatedAt',
        updatedAt: 'UpdatedAt'
    }
});

module.exports = Patient;