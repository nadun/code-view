const patientLogic = require('../logic/patient');
const paramsUtils = require('../util/params');
const responseUtils = require('../util/response');
const validation = require('../validation');
const log = require('../util/logging');

const create = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false;
    let user_id, user_email;
    
    try{
        const body = paramsUtils.getBody(event);

        if (isHttp) {
            let user = event.requestContext.authorizer.claims;
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
                user_id = user.sub
                user_email = user.email
            } else {
                user_id = user.sub
                user_email = user.email
            }
        } else {
            user_email = body.user_email
            user_id = body.user_id
        }

        log.info(`body: ${JSON.stringify(body)})`);

        const {
            name,
            email,
            phone_number,
            case_date,
            appointment_date,
            age,
            gender,
            birthday,
            address,
            comments,
            created_by,
        } = body;
        
        //validating user attributes
        await validation.validate(body, '/Patient')

        //creating patient
        let result = await patientLogic.create(name, email, phone_number, case_date, appointment_date, age, gender, birthday, address, comments, user_id, user_email)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);
    
        return response
    }
    
};

const edit = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false;
    let user_id, user_email;
    const body = paramsUtils.getBody(event);

    if (isHttp) {
        let user = event.requestContext.authorizer.claims;
        if (paramsUtils.isString(user)) {
            user = JSON.parse(user);
            user_id = user.sub
            user_email = user.email
        } else {
            user_id = user.sub
            user_email = user.email
        }
    } else {
        user_email = body.user_email
        user_id = body.user_id;
    }
    
    try{
        

        log.info(`body: ${JSON.stringify(body)})`);

        const {
            name,
            email,
            phone_number,
            case_date,
            appointment_date,
            age,
            gender,
            birthday,
            address,
            comments,
            patient_id
        } = body;
        
        //validating user attributes
        await validation.validate(body, '/Patient')

        //creating patient
        let result = await patientLogic.edit(patient_id, name, email, phone_number, case_date, appointment_date, age, gender, birthday, address, comments, user_id)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);
    
        return response
    }
    
};

const get = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false;

    try{
        
        let user_id, user_email;

        const body = paramsUtils.getBody(event);

        if (isHttp) {
            let user = event.requestContext.authorizer.claims;
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
                user_id = user.sub
                user_email = user.email
            } else {
                user_id = user.sub
                user_email = user.email
            }
        } else {
            user_email = body.email,
            user_id = body.user_id
        }
        

        log.info(`body: ${JSON.stringify(body)})`);

        const {
            patient_id
        } = body;

        //creating patient
        let result = await patientLogic.getPatient(patient_id, user_id)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);
    
        return response
    }
}

const getAll = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false;
    try{
        
        const body = paramsUtils.getBody(event);
        let user_id, user_email;

        if (isHttp) {
            let user = event.requestContext.authorizer.claims;
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
                user_id = user.sub
                user_email = user.email
            } else {
                user_id = user.sub
                user_email = user.email
            }
        } else {
            user_email = body.email,
            user_id = body.user_id
        }
        
        const { limit, last_key } = body

        log.info(`body: ${JSON.stringify(body)})`, user_id);

        //get patients by doctor
        let result = await patientLogic.getAll(limit, last_key, user_id)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);
    
        return response
    }
}

module.exports = {
    create,
    edit,
    get,
    getAll
};
