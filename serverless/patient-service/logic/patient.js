const log = require('../util/logging');
const patientRepo = require('../repository/repo')
const uuidv4 = require("uuid/v4")

class Patient {

    create(name, email, phone_number, case_date, appointment_date, age, gender, birthday, address, comments, created_by, created_by_email) {
        return new Promise((resolve, reject) => {
            let newPatient = new patientRepo({
                PatientId: uuidv4(),
                Name: name,
                Email: email,
                PhoneNumber: phone_number,
                CaseDate: case_date,
                AppointmentDate: appointment_date,
                Age: age,
                Gender: gender,
                Birthday: birthday,
                Address: address,
                Comments: comments,
                CreatedBy: created_by,
                CreatedByEmail: created_by_email
            })

            newPatient.save(err => {
                if (err) {
                    log.error(err)
                    reject(err)
                }

                resolve("patient create successful")
            })
        })

    }

    edit(patient_id, name, email, phone_number, case_date, appointment_date, age, gender, birthday, address, comments, doctor_id) {

        let _self = this;

        return new Promise(async (resolve, reject) => {

            try {

                let res = await _self.getPatient(patient_id, doctor_id)

                if (res === "cannot find the patient") {
                    log.error(res)
                    throw new Error(res)
                }

                let updateObj = {}

                if (name) {
                    updateObj["Name"] = name;
                }

                if (email) {
                    updateObj["Email"] = email;
                }

                if (phone_number) {
                    updateObj["PhoneNumber"] = phone_number;
                }

                if (case_date) {
                    updateObj["CaseDate"] = case_date;
                }

                if (appointment_date) {
                    updateObj["AppointmentDate"] = appointment_date;
                }

                if (age) {
                    updateObj["Age"] = age;
                }

                if (gender) {
                    updateObj["Gender"] = gender;
                }

                if (birthday) {
                    updateObj["Birthday"] = birthday;
                }

                if (address) {
                    updateObj["Address"] = address;
                }

                if (comments) {
                    updateObj["Comments"] = comments;
                }

                patientRepo.update({PatientId: patient_id}, updateObj, function (err) {
                    if (err) {
                        return reject(err)
                    }

                    resolve("update success")
                });

            } catch (err) {
                log.error(err)
                return reject(err)
            }

        })

    }

    getAll(limit, last_key, doctor_id) {
        limit = limit || 100;
        last_key = last_key || null
        return new Promise((resolve, reject) => {

            let query = {};

            if (limit) {

                if (last_key != null) {
                    query = patientRepo.scan({CreatedBy: doctor_id}).startAt(last_key).limit(limit)
                } else {
                    query = patientRepo.scan({CreatedBy: doctor_id}).limit(limit)
                }
            }

            query.exec((err, result) => {
                if (err) {
                    return reject(err)
                }

                let lastKey = result.lastKey || null
                resolve({data: result, last_key: lastKey})
            })
        })
    }

    getPatient(patient_id, user_id) {
        return new Promise((resolve, reject) => {
            patientRepo.get({PatientId: patient_id}, (err, patient) => {
                if (err) {
                    log.error(err)
                    return reject(reject)
                }


                if (patient) {

                    if (patient.CreatedBy == user_id) {
                        return resolve(patient)
                    }
                }

                log.error("cannot find the patient")
                return reject("cannot find the patient")

            })
        })
    }

}

module.exports = new Patient()
