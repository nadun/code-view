module.exports = {
    Patient: {
        id: '/Patient',
        type: 'object',
        properties: {
            name: { type: 'string' },
            email: { type: 'string' },
            phone_number: { type: 'string' },
            case_date: { type: 'string' },
            appointment_date: { type: 'string' },
            age: { type: 'number' },
            gender: { type: 'string' },
            birthday: { type: 'string' },
            address: { type: 'string' },
            comments: { type: 'string' },
            created_by: { type: 'string' },
        },
        required: ['name', 'email', 'age', 'gender', 'birthday']
    }
};
