const log = require('../util/logging');
const userRepo = require('../repository/repo')

const AWS = require("aws-sdk");
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool;
const CognitoUserAttribute = AmazonCognitoIdentity.CognitoUserAttribute;

class User{

    create(user, user_name){
        return new Promise((resolve, reject) => {
            let newUser = new userRepo({
                UserId: user.sub,
                Name: user.name,
                UserName: user_name,
                Email: user.email,
                EmailVerified: user.email_verified,
                MFA:{
                    Login: "Enable"
                },
                CognitoRegisteredDateTime: new Date().toLocaleString(),
                PhoneNumber: user.phone_number,
                FamilyName: user.family_name,
                Clinic: user["custom:custom:clinic"],
                ClinicType: user["custom:clinicType"],
                DialCode: user["custom:dialCode"],
                MobileNumber: user["custom:mobile_number"],
                Language: user["custom:custom:language"],
                Country: user["custom:custom:country"]
            })

            newUser.save(err => {
                if(err){
                    log.error(err)
                    reject(err)
                }

                resolve("user create successful")
            })
        })


    }

    getAll(limit, last_key){
        limit = limit || 100;
        last_key = last_key || null
        return new Promise((resolve, reject) => {

            let query = {};

            if(limit){

                if(last_key != null){
                    query = userRepo.scan().startAt(last_key).limit(limit)
                }else{
                    query = userRepo.scan().limit(limit)
                }
            }

            query.exec((err, result) => {
                if(err){
                    return reject(err)
                }

                let lastKey = result.lastKey || null
                resolve({data: result, last_key: lastKey})
            })
        })
    }

    getUserById(user_id){
        return new Promise((resolve, reject) => {
            userRepo.get({UserId: user_id}, function(err, user) {
                if(err) { return reject(err) }
                resolve(user)
              });
        })
    }

    updateCustomerId(user_id, customer_id){
        return new Promise((resolve, reject) => {

            userRepo.update({UserId: user_id}, {$PUT: {CustomerId: customer_id}}, function(err) {
                if(err) { return reject(err) }
                resolve("update success")
            });
        })
    }

    updateInfusionsoftId(user_id, infusionsoft_id){
        return new Promise((resolve, reject) => {

            userRepo.update({UserId: user_id}, {$PUT: {InfusionsoftId: infusionsoft_id}}, function(err) {
                if(err) { return reject(err) }
                resolve("update success")
            });
        })
    }


    update(user_id, username, name, email, family_name, language, country, clinic, clinic_type, dial_code, mobile_number){

        return new Promise((resolve, reject) => {

            let attributes = [];
            let updateAttribute = {}

            if(name){
                attributes.push({
                    Name: 'name',
                    Value: name
                })

                updateAttribute["Name"] = name
            }

            if(family_name){
                attributes.push({
                    Name: 'family_name',
                    Value: family_name
                })

                updateAttribute["FamilyName"] = family_name
            }

            if(email){
                attributes.push({
                    Name: 'email',
                    Value: email
                })

                updateAttribute["Email"] = email
            }

            if(language){
                attributes.push({
                    Name: 'custom:custom:language',
                    Value: language
                })

                updateAttribute["Language"] = language
            }

            if(clinic){
                attributes.push({
                    Name: 'custom:custom:clinic',
                    Value: clinic
                })

                updateAttribute["Clinic"] = clinic
            }

            if(country){
                attributes.push({
                    Name: 'custom:custom:country',
                    Value: country
                })

                updateAttribute["Country"] = country
            }

            if(clinic_type){
                attributes.push({
                    Name: 'custom:clinicType',
                    Value: clinic_type
                })

                updateAttribute["ClinicType"] = clinic_type
            }

            if(dial_code && mobile_number){
                attributes.push({
                    Name: 'custom:dialCode',
                    Value: dial_code
                })

                updateAttribute["DialCode"] = dial_code

                attributes.push({
                    Name: 'custom:mobile_number',
                    Value: mobile_number
                })

                updateAttribute["MobileNumber"] = mobile_number

                attributes.push({
                    Name: 'phone_number',
                    Value: dial_code+mobile_number
                })

                updateAttribute["PhoneNumber"] = dial_code+mobile_number
            }

            let params = {
                UserAttributes: attributes,
                UserPoolId: process.env.userPoolId, /* required */
                Username: username
              };

            cognitoidentityserviceprovider.adminUpdateUserAttributes(params, async function(err, data) {
                if (err) {
                    log.error(err)
                    return reject(err)
                }

                userRepo.update({UserId: user_id}, updateAttribute, function(err) {
                    if(err) { return reject(err) }
                    resolve("update success")
                });
            });

        })
    }

    deleteUser(user_id, username){

        return new Promise((resolve, reject) => {

            let params = {
                UserPoolId: process.env.userPoolId,
                Username: username
            };

            cognitoidentityserviceprovider.adminDeleteUser(params, function(err, data) {
                if (err) {
                    log.error(err)
                    return reject(err)
                }

                userRepo.update({UserId: user_id}, {$PUT: {Status: "Deleted"}}, function(err) {
                    if(err) { return reject(err) }
                    resolve("delete successful")
                });

            });
        })

    }

    signupUser(username, password, email, name){
        let uname = username || email
        name = name || email || username
        return new Promise((resolve, reject) => {
             // Define AWS Cognito User Pool
            let poolData = {
                "UserPoolId": process.env.userPoolId,
                "ClientId": process.env.clientId
            };
            let userPool = new CognitoUserPool(poolData);

            // Define User Attributes
            let attributeList = [];

            let dataEmail = {
                "Name": "email",
                "Value": email
            };

            let dataPhoneNumber = {
                Name: 'phone_number',
                Value: '+15555555555',
            };

            let dataFamilyName = {
                Name: 'family_name',
                Value: '-',
            };

            let dataName = {
                Name: 'name',
                Value: name,
            };

            let attributefamilyName = new CognitoUserAttribute(dataFamilyName);
            let attributePhone = new CognitoUserAttribute(dataPhoneNumber);
            let attributeEmail = new CognitoUserAttribute(dataEmail);
            let attributeName = new CognitoUserAttribute(dataName);

            attributeList.push(attributeEmail);
            attributeList.push(attributePhone);
            attributeList.push(attributefamilyName);
            attributeList.push(attributeName);

            log.info('attributeList:',attributeList);

            // Create User via AWS Cognito
            userPool.signUp(uname, password, attributeList, null, function(err, result) {
                if(err) {
                    log.error(err);
                    return reject(err);
                } else {
                    log.info('result:',result);
                    let cognitoUser = result.user;
                    log.info('user name is ' + cognitoUser.getUsername());
                    resolve(result);
                }
            });
        })
    }

}

module.exports = new User()
