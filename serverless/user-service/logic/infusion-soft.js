const axios = require('axios')
const log = require('../util/logging')

class InfusionSoft{

    create(name, email, family_name, phone_number){
        return new Promise((resolve, reject) => {

            let params = {
                "email_addresses": [
                    {
                        "email": email,
                        "field": "EMAIL1"
                    }
                ],
                "family_name": family_name,
                "given_name": name,
                "phone_numbers": [
                    {
                        "field": "PHONE1",
                        "number": phone_number
                    }
                ]
            }

            axios.post(process.env.infusionsoft_api_url+process.env.infusionsoft_api_key, params)
            .then(result => {
                resolve(result)
            })   
            .catch(err => {
                log.error(err)
                return reject(err)
            })
        })
         
    }

}

module.exports = new InfusionSoft()