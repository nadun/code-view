const log = require('../util/logging');
global.fetch = require('node-fetch');
global.navigator = () => null;

const AWS = require("aws-sdk");
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');


class Cognito {

    login(username, password){

        return new Promise((resolve, reject) => {
            let authenticationData = {
                Username: username,
                Password: password,
            };
        
            let authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(
                authenticationData
            );
        
            let poolData = {
                UserPoolId: process.env.userPoolId, // Your user pool id here
                ClientId: process.env.clientId, // Your client id here
            };
        
            let userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
        
            let userData = {
                Username: username,
                Pool: userPool,
            };
        
            let cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

            cognitoUser.authenticateUser(authenticationDetails, {
                onSuccess: function(result) {
                    var accessToken = result
                    resolve(accessToken)
                },
                onFailure: function(err) {
                    log.error(err)
                    reject(err)
                },
                newPasswordRequired: function(userAttributes, requiredAttributes, session) {
                    log.info(userAttributes, session)

                    delete userAttributes.email_verified;

                    resolve({ChallengeName: "NEW_PASSWORD_REQUIRED", userAttributes, session: cognitoUser.Session})
                }
            });
        })
        
    }

    logout(username){

        return new Promise(async (resolve, reject) => {
        
            let params = {
                UserPoolId: process.env.userPoolId, /* required */
                Username: username /* required */
            };
            cognitoidentityserviceprovider.adminUserGlobalSignOut(params, function(err, data) {
                if (err) {
                    log.error(err)
                    reject(err)
                }
                else {
                    resolve("logged out")
                }
            });

            
        })  
        
    }

}

module.exports = new Cognito()