const dynamoose = require('./connector-dynamodb');
const schema = require('./schema');

const repository = dynamoose.model('Users', schema);

module.exports = repository;
