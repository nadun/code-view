module.exports = {
    calculation: {
        id: '/User',
        type: 'object',
        properties: {
            sub: { type: 'string' },
            email_verified: { type: 'string' },
            name: { type: 'string' },
            email: { type: 'string' },
        },
        required: ['sub', 'name', 'email']
    }
};
