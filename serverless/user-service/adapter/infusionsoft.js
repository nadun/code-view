const log = require('../util/logging');
const lambda = require('../util/lambda');

const create = async (user_id, name, email, family_name, phone_number,registration_date) => {
    return await lambda.callLambdaAsyncPromise(`infusion-soft-service-${process.env.stage}-publishToSns`, {
        message_type:'INFUSIONSOFT_CONTACT',
        message:{
            user_id: user_id,
            name: name,
            email: email,
            family_name: family_name,
            phone_number: phone_number,
            registration_date:registration_date
       }
    })
}

module.exports = {
    create
}
