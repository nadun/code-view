const userLogic = require('../logic/user');
const infusionAdapter = require('../adapter/infusionsoft');
const paramsUtils = require('../util/params');
const responseUtils = require('../util/response');
const validation = require('../validation');
const log = require('../util/logging');
const moment = require('moment');

const create = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false

    try{
        const body = paramsUtils.getBody(event);

        log.info(`body: ${JSON.stringify(body)})`);

        let user = body.request.userAttributes

        //validating user attributes
        await validation.validate(user, '/User')

        //creating user
        await userLogic.create(user, body.userName)

        //infusion soft contact create
        await infusionAdapter.create(user.sub, user.name, user.email, user.family_name, user.phone_number, moment().format("YYYY-MM-DD"))

        // const response = responseUtils.successResponse({
        //     result,
        //     development: process.env.development,
        //     region: process.env.region
        // },isHttp);

        callback(null, event);

    }catch(err){
        log.error("--create user error--", err)
        const response = responseUtils.errorResponse(400, err.message, isHttp);

        callback(null, response);
    }

};

const updateCustomerId = async (event, context, callback) => {
    try{
        const isHttp = (event.httpMethod) ? true : false
        const body = paramsUtils.getBody(event);

        log.info(`body: ${JSON.stringify(body)})`);

        let { user_id, customer_id } = body

        //creating user
        let result = await userLogic.updateCustomerId(user_id, customer_id)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message);

        callback(null, response);
    }

};

const updateInfusionsoftId = async (event, context, callback) => {
    try{
        const isHttp = (event.httpMethod) ? true : false
        const body = paramsUtils.getBody(event);

        log.info(`body: ${JSON.stringify(body)})`);

        let { user_id, infusionsoft_id } = body

        //creating user
        let result = await userLogic.updateInfusionsoftId(user_id, infusionsoft_id)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message);

        callback(null, response);
    }

};

const update = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    const body = paramsUtils.getBody(event);
    let user_id, user_email, username;
    try{
        log.info(`body: ${JSON.stringify(body)})`);

        if (isHttp) {
            let user = event.requestContext.authorizer.claims;
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
                user_id = user.sub
                user_email = user.email
                username = user["cognito:username"]
            } else {
                user_id = user.sub
                user_email = user.email
                username = user["cognito:username"]
            }
        } else {
            user_email = body.user_email
            user_id = body.user_id
            username = body.username
        }

        let { name, email, family_name, language, country, clinic, clinic_type, dial_code, mobile_number } = body

        //edit user
        let result = await userLogic.update(user_id, username, name, email, family_name, language, country, clinic, clinic_type, dial_code, mobile_number)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);

        callback(null, response);
    }

};

const updateUser = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    const body = paramsUtils.getBody(event);
    try{
        log.info(`body: ${JSON.stringify(body)})`);

        let { user_id, username, name, email, family_name, language, country, clinic, clinic_type, dial_code, mobile_number } = body

        //edit user
        let result = await userLogic.update(user_id, username, name, email, family_name, language, country, clinic, clinic_type, dial_code, mobile_number)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);

        callback(null, response);
    }

};

const getAll = async (event, context, callback) => {
    try{
        const isHttp = (event.httpMethod) ? true : false
        const body = paramsUtils.getBody(event);

        log.info(`body: ${JSON.stringify(body)})`);

        let {limit, last_key} = body

        //creating user
        let result = await userLogic.getAll(limit, last_key)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message);

        callback(null, response);
    }

};

const deleteUser = async (event, context, callback) => {
    try{
        const isHttp = (event.httpMethod) ? true : false
        const body = paramsUtils.getBody(event);

        log.info(`body: ${JSON.stringify(body)})`);

        let { user_id, username } = body

        //delete user
        let result = await userLogic.deleteUser(user_id, username)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message);

        callback(null, response);
    }

};

const getUserById = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    const body = paramsUtils.getBody(event);
    let user_id, user_email;

    try{

        if (isHttp) {
            let user = event.requestContext.authorizer.claims;
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
                user_id = user.sub
                user_email = user.email
            } else {
                user_id = user.sub
                user_email = user.email
            }
        } else {
            user_email = body.user_email
            user_id = body.user_id
        }

        log.info(`body: ${JSON.stringify(body)})`);

        //get user
        let result = await userLogic.getUserById(user_id)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);

        callback(null, response);
    }

};

const getUser = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    const body = paramsUtils.getBody(event);
    try{

        log.info(`body: ${JSON.stringify(body)})`);
        let user_id = null

        if(isHttp){
            user_id = event.pathParameters.user_id; 
        }else{
            user_id = body.user_id
        }
        //get user
        let result = await userLogic.getUserById(user_id)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);
    
        callback(null, response);
    }
    
};

module.exports = {
    create,
    getAll,
    deleteUser,
    getUserById,
    getUser,
    updateCustomerId,
    updateInfusionsoftId,
    updateUser,
    update
};
