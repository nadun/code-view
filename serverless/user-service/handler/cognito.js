const cognitoLogic = require('../logic/cognito');
const paramsUtils = require('../util/params');
const responseUtils = require('../util/response');
const validation = require('../validation');
const log = require('../util/logging');

const login = async (event, context, callback) => {

    const isHttp = (event.httpMethod) ? true : false
    
    try{
        const body = paramsUtils.getBody(event);
        log.info(`body: ${JSON.stringify(body)})`);

        //validating login attributes
        await validation.validate(body, '/Login')

        const { username, password } = body;

        //
        let result = await cognitoLogic.login(username, password);

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        log.error("--login error--", err)
        const response = responseUtils.errorResponse(400, err.message, isHttp);
    
       return response;
    }

}

const logout = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false

    const body = paramsUtils.getBody(event);

    const { session } = body

    let user_email, username;
    try{
        log.info(`body: ${JSON.stringify(body)})`);

        if (isHttp) {
            let user = event.requestContext.authorizer.claims;

            log.info(user)
            
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
                user_email = user.email || null
                username = user["cognito:username"] || null
            } else {
                user_email = user.email || null
                username = user["cognito:username"] || null
            }
        } else {
            user_email = body.user_email
            username = body.username
        }

        let data = await cognitoLogic.logout(username, session)

        const response = responseUtils.successResponse({
            data,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response
    }
    catch(err){
        log.error(err)

        const response = responseUtils.errorResponse(400, err.message, isHttp);
        return response
    }
}

module.exports = {
    login,
    logout
}