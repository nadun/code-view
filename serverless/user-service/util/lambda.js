const log = require('../util/logging');
const AWS = require('aws-sdk');

const callLambda = (functionName, jsonBody, callback) => {
    const lambda = new AWS.Lambda();

    const params = {
        FunctionName: functionName,
        Payload: JSON.stringify(jsonBody)
    };

    log.info(`Will call lambda ${functionName} with ${JSON.stringify(params)}`);

    lambda.invoke(params, (err, result) => {
        if (err) {
            log.error(`Error invoking lambda function ${functionName}, with: ${JSON.stringify(jsonBody)}`);
            callback(err, null);
            return;
        }

        const httpPayload = JSON.parse(result.Payload);
        const httpPayloadBody = httpPayload.body;

        log.info('HttpPayload: ', httpPayload);
        log.info('HttpPayload.body: ', httpPayloadBody);
        log.info('HttpPayload.body type: ', typeof httpPayloadBody);

        let lambdaResponse;
        if (typeof httpPayloadBody === 'object') {
            lambdaResponse = httpPayload.body;
        } else {
            lambdaResponse = JSON.parse(httpPayload.body);
        }

        callback(null, lambdaResponse);
    });
};


const callLambdaAsyncPromise = (functionName, jsonBody) => {

    return new Promise((resolve, reject) => {
        const lambda = new AWS.Lambda();

        const params = {
            FunctionName: functionName,
            InvocationType: 'Event',
            Payload: JSON.stringify(jsonBody)
        };
    
        log.info(`Will call lambda ${functionName} with ${JSON.stringify(params)}`);
    
        lambda.invoke(params, (err, result) => {
            if (err) {
                log.error(`Error invoking lambda function ${functionName}, with: ${JSON.stringify(jsonBody)}`);
                reject(err);
            }
    
            resolve(result.Payload)
    
        });
    })

};

module.exports = {
    callLambda,
    callLambdaAsyncPromise
};
