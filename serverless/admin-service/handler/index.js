const userLogic = require('../logic/user');
const paramsUtils = require('../util/params');
const responseUtils = require('../util/response');
const validation = require('../validation');
const log = require('../util/logging');

const login = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false

    const body = paramsUtils.getBody(event);

    const { username, password } = body

    try{
        let data = await userLogic.login(username, password)

        const response = responseUtils.successResponse({
            data,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response
    }
    catch(err){
        log.error(err)

        const response = responseUtils.errorResponse(400, err.message, isHttp);
        return response
    }

}

const newPassword = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false

    const body = paramsUtils.getBody(event);

    const { username, new_password, session } = body

    try{
        let data = await userLogic.newPassword(username, new_password, session)

        const response = responseUtils.successResponse({
            data,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response
    }
    catch(err){
        log.error(err)

        const response = responseUtils.errorResponse(400, err.message, isHttp);
        return response
    }
}

const signupUser = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    try{
        const body = paramsUtils.getBody(event);

        log.info(`body: ${JSON.stringify(body)})`);

        const { username, password, email, display_name } = body;

        //creating user
        let result = await userLogic.signupUser(username, password, email, display_name)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);
    
        callback(null, response);
    }
    
}

const listUsers = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    try{
        const body = paramsUtils.getBody(event);

        log.info(`body: ${JSON.stringify(body)})`);

        //creating user
        let result = await userLogic.listUsers()

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);
    
        callback(null, response);
    }
    
}

const getUser = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    try{
        let user_id = null
        const body = paramsUtils.getBody(event);

        if(isHttp){
            user_id = event.pathParameters.user_id; 
        }else{
            user_id = body.user_id
        }

        log.info(`body: ${JSON.stringify(body)})`);

        //creating user
        let result = await userLogic.getUser(user_id)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);
    
        callback(null, response);
    }
    
}

const deleteUser = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    try{
        const body = paramsUtils.getBody(event);

        log.info(`body: ${JSON.stringify(body)})`);

        const { username } = body;

        //deleting user
        let result = await userLogic.deleteUser(username)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);
    
        callback(null, response);
    }
    
}

const editUser = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    try{
        const body = paramsUtils.getBody(event);

        log.info(`body: ${JSON.stringify(body)})`);
        let username = null

        if(isHttp){
            username = event.pathParameters.user_name; 
        }else{
            username = body.user_name
        }

        const { email, display_name, user_group } = body;

        //edit user
        let result = await userLogic.editUser(username, email, display_name, user_group)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);
    
        callback(null, response);
    }
    
}







module.exports = {
    login,
    newPassword,
    signupUser,
    listUsers,
    deleteUser,
    editUser,
    getUser
};
