const log = require('../util/logging');
const lambdaEmail = require('../util/lambda-email');

global.fetch = require('node-fetch');
global.navigator = () => null;

const AWS = require("aws-sdk");
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');

class User{

    login(username, password){

        return new Promise((resolve, reject) => {
            let authenticationData = {
                Username: username,
                Password: password,
            };
        
            let authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(
                authenticationData
            );
        
            let poolData = {
                UserPoolId: process.env.userPoolId, // Your user pool id here
                ClientId: process.env.clientId, // Your client id here
            };
        
            let userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
        
            let userData = {
                Username: username,
                Pool: userPool,
            };
        
            let cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

            cognitoUser.authenticateUser(authenticationDetails, {
                onSuccess: function(result) {
                    var accessToken = result
                    resolve({ChallengeName: "OTP_REQUIRED", token: accessToken})
                },
                onFailure: function(err) {
                    log.error(err)
                    reject(err)
                },
                newPasswordRequired: function(userAttributes, requiredAttributes, session) {
                    log.info(userAttributes, session)

                    delete userAttributes.email_verified;

                    resolve({ChallengeName: "NEW_PASSWORD_REQUIRED", username: username, userAttributes, session: cognitoUser.Session})
                }
            });
        })
        
    }

    newPassword(username, new_password, session){

        return new Promise((resolve, reject) => {
            let poolData = {
                UserPoolId: process.env.userPoolId, // Your user pool id here
                ClientId: process.env.clientId, // Your client id here
            };
        
            let userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
        
            let userData = {
                Username: username,
                Pool: userPool,
            };
        
            let cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

            cognitoUser["Session"] = session

            cognitoUser.completeNewPasswordChallenge(new_password, {email: username}, {
                onSuccess: function(result) {
                    var accessToken = result
                    resolve(accessToken)
                },
                onFailure: function(err) {
                    log.error(err)
                    reject(err)
                },
                mfaSetup: function(challengeName, challengeParameters) {
                    cognitoUser.associateSoftwareToken(this);
                },
                associateSecretCode: function(secretCode) {
                    var challengeAnswer = 'Please input the TOTP code.';
                    cognitoUser.verifySoftwareToken(challengeAnswer, 'My TOTP device', this);
                },
            });
        })  
        
    }

    signupUser(username, password, email, name, dispay_name){
        let uname = username || email
        name = name || email || username
        dispay_name = dispay_name || name
        return new Promise((resolve, reject) => {
            
            var params = {
                UserPoolId: process.env.userPoolId,
                Username: uname,
                DesiredDeliveryMediums: ["EMAIL"],
                ForceAliasCreation: false,
                MessageAction: "SUPPRESS",
                TemporaryPassword: password,
                UserAttributes: [
                    { Name: "given_name", Value: uname },
                    { Name: "email", Value: email },
                    { Name: "email_verified", Value: "true" },
                    { Name: "custom:display_name", Value: dispay_name }
                ]
            };

            

            cognitoidentityserviceprovider.adminCreateUser(params, async function(err, data) {
                if (err) {
                    reject(err);
                } else {
                    
                    await lambdaEmail.sendEmail(email, "DSD Admin User Credentials", `<p>dear user.please log in to admin panel using below credentials.</p><p>username: ${uname} </p><p>password: ${password}`)
                    resolve(data);
                }
            });
        })
    }

    listUsers(){

        return new Promise((resolve, reject) => {

            var params = {
                UserPoolId: process.env.userPoolId, /* required */
                AttributesToGet: [
                "email",
                "sub"
                ],
                Limit: 0
            };

            cognitoidentityserviceprovider.listUsers(params, function (err, data) {
                if (err) {
                    log.error(err)
                    return reject(err)
                }
                
                resolve(data);
            });

        })
    }

    getUser(user_id){

        return new Promise((resolve, reject) => {

            var params = {
                UserPoolId: process.env.userPoolId, /* required */
                Username: user_id
            };

            cognitoidentityserviceprovider.adminGetUser(params, function (err, data) {
                if (err) {
                    log.error(err)
                    return reject(err)
                }
                
                resolve(data);
            });

        })
    }

    

    deleteUser(username){
         
        return new Promise((resolve, reject) => {

            var params = {
                UserPoolId: process.env.userPoolId, 
                Username: username 
              };
              cognitoidentityserviceprovider.adminDeleteUser(params, function(err, data) {
                if (err) {
                    log.error(err)
                    return reject(err)
                }
                
                resolve("delete successful")
              });
        })
    }

    editUser(username, email, display_name, user_group){

        let _self = this;
         
        return new Promise((resolve, reject) => {

            let attributes = [];

            if(email){
                attributes.push({
                    Name: 'email',
                    Value: email
                })
            }

            if(display_name){
                attributes.push({
                    Name: 'custom:display_name',
                    Value: display_name
                })
            }

            let params = {
                UserAttributes: attributes,
                UserPoolId: process.env.userPoolId, /* required */
                Username: username
              };

            cognitoidentityserviceprovider.adminUpdateUserAttributes(params, async function(err, data) {
                if (err) {
                    log.error(err)
                    return reject(err)
                }

                if(user_group == "admin" || user_group == "marketing"){

                    let params = {
                        GroupName: user_group, /* required */
                        UserPoolId: process.env.userPoolId,
                        Username: username
                    };

                    let groups = await _self.getUserGroups(username)

                    await _self.removeGroupsFromUser(username, groups.Groups)


                    cognitoidentityserviceprovider.adminAddUserToGroup(params, function(err, data) {
                        if (err) {
                            log.error(err)
                            return reject(err)
                        }
                        else {
                            resolve("admin updated with user group")
                        }
                    });

                }else{
                    resolve("admin updated")
                }

            });

        })
    }

    getUserGroups(username){

        return new Promise((resolve, reject) => {
            let params = {
                UserPoolId: process.env.userPoolId, 
                Username: username
            };
    
            cognitoidentityserviceprovider.adminListGroupsForUser(params, function(err, data) {
                if (err) {
                    log.error(err)
                    return reject(err)
                }    

                return resolve(data)
            });
        })

        
    }

    async removeGroupsFromUser(username, groups){
        
        for (const key in groups) {
            const element = groups[key];

            await this.removeGroupFromUser(username, element.GroupName)
        }

        return "all groups removed"

    }

    removeGroupFromUser(username, group_name){
        return new Promise((resolve, reject) => {

            var params = {
                GroupName: group_name,
                UserPoolId: process.env.userPoolId, 
                Username: username
            };
            cognitoidentityserviceprovider.adminRemoveUserFromGroup(params, function(err, data) {
                if (err) {
                    log.error(err)
                    return reject(err)
                }   

                return resolve(data)
            });
        })
    }
    

}

module.exports = new User()