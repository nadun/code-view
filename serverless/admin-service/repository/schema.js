const dynamoose = require('./connector-dynamodb');

const { Schema } = dynamoose;

const MFAOptions = ["Enable", "Disable"];

var User = new Schema({
    UserId: {
        type: String,
        hashKey: true
    },
    CognitoRegisteredDateTime: {
        type: String
    },
    Name: String,
    UserName: String,
    Email: {
        type: String,
        index: {
            global: true,
            name: 'Email-Index',
            throughput: {
                read: 2,
                write: 2
            } // read and write are both 1
        },
    },
    EmailVerified: {
        type: Boolean,
        default: false
    },
    MFA: {
        type: 'map',
        map: {
            Login: {
                type: String,
                default: "Enable",
                validate: function (item) {
                    return MFAOptions.indexOf(item) >= 0;
                }
            }
        }
    },
    CustomerId:{
        type: String
    }
}, {
    useDocumentTypes: true,
    throughput: {
        read: 2,
        write: 2
    },
    timestamps: {
        createdAt: 'CreatedAt',
        updatedAt: 'UpdatedAt'
    }
});

module.exports = User;
