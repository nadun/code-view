const dynamoose = require('dynamoose');

const dynamooseConfigString = process.env.dynamoose || '{"create":true,"local":true}';
const dynamooseConfig = JSON.parse(dynamooseConfigString);

dynamoose.AWS.config.update({
    region: process.env.region || 'us-west-2'
});

dynamoose.setDefaults({ create: dynamooseConfig.create });

if (dynamooseConfig.local === true) {
    // dynamoose.local();
}

module.exports = dynamoose;
