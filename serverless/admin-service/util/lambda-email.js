const AWS = require('aws-sdk');

const lambda = new AWS.Lambda();
const sendEmail = function(email, subject, emailBody){
    var params = {
        FunctionName: `email-service-${process.env.stage}-email`,
        InvocationType: 'Event',
        Payload: JSON.stringify({
            "fromAddress": "DSD <"+process.env.notifyEmail+">",
            "toAddresses": [
                email
            ],
            "subject": subject,
            "emailBody":emailBody
        })
    };

    let promise = lambda.invoke(params).promise();
    return promise;
}

module.exports = {
    sendEmail
};