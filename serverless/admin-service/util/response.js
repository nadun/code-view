const log = require('./logging');

const successResponseHttp = (data) => {
    log.info('Output: ', data);

    let resp = null;
    
    if(typeof data == "string"){
        resp = {data : data}
    }else{
        resp = data
    }

    return {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify({
            status: 200,
            resp
        })
    };
};

const errorResponseHttp = (code, err) => {
    log.error('Error: ', err);

    return {
        statusCode: code || 400,
        headers: {
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify({
            status: code || 400,
            err
        })
    };
};

const successResponse = (data, isHttp) => {
    log.info('Output: ', data);

    if(isHttp){
        return successResponseHttp(data)
    }else{
        return {
            statusCode: 200,
            body: {
                data: data || null
            }
        };
    }

};

const errorResponse = (code, err, isHttp) => {
    log.error('Error: ', err);

    if(isHttp){
        return errorResponseHttp(code, err)
    }else{
        return {
            statusCode: code || 400,
            body: {
                err: err || null
            }
        };
    }
};

const sendError = (message, callback) => {
    // const err = {
    //     error_code: 'service_unavailable',
    //     message
    // };

    callback(message);
};

module.exports = {
    successResponseHttp,
    errorResponseHttp,
    successResponse,
    errorResponse,
    sendError
};
