const { Validator } = require('jsonschema');
const fs = require('fs');

class Validation {
    constructor() {
        this.v = new Validator();
        const self = this;
        const pathSchema = Validation._validationSchemaPath();
        const files = fs.readdirSync(pathSchema);
        files.forEach((file) => {
            if (file && file.match('.js')) {
                const schema = require(`${pathSchema}/${file}`); // eslint-disable-line
                self.v.addSchema(schema, schema.id);
            }
        });
    }

    validate(object, type) {
        const schema = this.v.schemas[type];
        return new Promise((resolve, reject) => {
            const validation = this.v.validate(object, schema);
            if (validation.errors.length > 0) {
                reject(validation.errors);
            } else {
                resolve(object);
            }
        });
    }

    static _validationSchemaPath() {
        return `${process.cwd()}/validation/schema`;
    }
}

module.exports = new Validation();
