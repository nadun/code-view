module.exports = {
    id: '/Email',
    type: 'object',
    properties: {
        fromAddress: { type: 'string', minLength: 1 },
        toAddresses: {
            anyOf: [
                {
                    type: 'string',
                    minLength: 1
                },
                {
                    type: 'array',
                    minItems: 1
                }
            ]
        },
        ccAddresses: {
            anyOf: [
                {
                    type: 'string',
                    minLength: 1
                },
                {
                    type: 'array',
                    minItems: 1
                }
            ]
        },
        bccAddresses: {
            anyOf: [
                {
                    type: 'string',
                    minLength: 1
                },
                {
                    type: 'array',
                    minItems: 1
                }
            ]
        },
        replyToAddresses: {
            anyOf: [
                {
                    type: 'string',
                    minLength: 1
                },
                {
                    type: 'array',
                    minItems: 1
                }
            ]
        },
        subject: { type: 'string' },
        body: { type: 'string' },
        path: { type: 'string' },
        substitute: { type: 'object' },
        reference: { type: 'string' }
    },
    required: ['fromAddress', 'toAddresses']
};