const log = require('./logging');
const aws = require('aws-sdk');
const request = require('request');

const s3 = new aws.S3();

class Template {
    getTemplate(path) {
        this.path = path;
        if (this.path.startsWith('http://') || this.path.startsWith('https://')) {
            return this.getHttpTemplate();
        }
        return this.getBucketTemplate();
    }

    getBucketTemplate() {
        return new Promise((resolve, reject) => {
            s3.getObject({ Bucket: process.env.emailBucket, Key: this.path }, (err, data) => {
                if (err) {
                    log.error('S3 error get template bucket', err, process.env.emailBucket, this.path);
                    return reject(err);
                }
                const templateBody = data.Body.toString();
                return resolve(templateBody);
            });
        });
    }

    getHttpTemplate() {
        return new Promise((resolve, reject) => {
            request(this.path, (error, response, body) => {
                if (error) {
                    log.error('Error get template http ', this.path, error);
                    return reject(error);
                } else if (response.statusCode === 200) {
                    return resolve(body);
                }
                return resolve({ message: body, statusCode: response.statusCode, path: this.path });
            });
        });
    }
}

module.exports = new Template();
