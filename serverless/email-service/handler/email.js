const log = require('../util/logging');
const _ = require('lodash');
const emailLogic = require('../logic/email');
const paramsUtils = require('../util/params');
const responseUtils = require('../util/response');
const validation = require('../validation');

const sendEmail = async (event, context, callback) => {
    const postData = paramsUtils.getBody(event);

    try{
        await validation.validate(postData, '/Email')

        let toAddresses = _.isArray(postData.toAddresses)? postData.toAddresses : _.castArray(postData.toAddresses)
        let ccAddresses = [];
        if(postData.ccAddresses) {
            ccAddresses = _.isArray(postData.ccAddresses) ? postData.ccAddresses : _.castArray(postData.ccAddresses)
        }
        let bccAddresses =[];
        if(postData.ccAddresses) {
            bccAddresses = _.isArray(postData.bccAddresses)? postData.bccAddresses : _.castArray(postData.bccAddresses);
        }

        let replyToAddresses =[];
        if(postData.replyToAddresses) {
            replyToAddresses = _.isArray(postData.replyToAddresses)? postData.replyToAddresses : _.castArray(postData.replyToAddresses)
        }

        const addressParam = {
            from: postData.fromAddress,
            to: toAddresses,
            cc: ccAddresses ,
            bcc: bccAddresses ,
            replyTo: replyToAddresses
        };
        const emailSubject = postData.subject || null;
        const emailBody = postData.emailBody || postData.body || null;

        if (!_.isNull(emailBody)) {
            log.debug('Sending email with message body');
            emailLogic.sendEmail(addressParam, emailSubject, emailBody)
                .then((result) => {
                    log.debug('emailLogic.sendEmail:success: ', result);
                    const response = responseUtils.successResponse( result );
                    callback(null, response);
                })
                .catch((err) => {
                    log.error('emailLogic.sendEmail:error: ', err);
                    const response = responseUtils.errorResponse(400, err);
                    callback(null, response);
                });
        } else{
            const response = responseUtils.errorResponse(400, {messsage:'need email body or email path'});
            callback(null, response);
        }
    }
    catch(err){
        log.error('err: ', err);
        const response = responseUtils.errorResponse(400, err.message);
        callback(null, response);
    }
    
};

module.exports = {
    sendEmail
};
