const log = require('../util/logging');
const repository = require('../repository/repo-email-log');
const _ = require('lodash');
const cheerio = require('cheerio');
const aws = require('aws-sdk');
const uuidV1 = require('uuid/v1');

const ses = new aws.SES({ region: 'eu-central-1' });

class LogicEmail {
    sendEmail(addressParam, subject, body) {
        let processedSubject = 'No Subject';

        return new Promise((resolve, reject) => {
            if (_.isNull(subject)) {
                const $ = cheerio.load(body);
                const title = $('title').text();
                if (!_.isEmpty(title.trim())) {
                    processedSubject = title.trim();
                }
            } else {
                processedSubject = subject;
            }

            const params = {
                Destination: {
                    ToAddresses: addressParam.to,
                    CcAddresses: addressParam.cc,
                    BccAddresses: addressParam.bcc
                },
                Message: {
                    Body: {
                        Html: {
                            Data: body
                        }
                    },
                    Subject: {
                        Data: processedSubject
                    }
                },
                Source: addressParam.from,
                ReplyToAddresses: addressParam.replyTo
            };

            ses.sendEmail(params, (err, result) => {
                let isSuccess = true;

                if (err) {
                    isSuccess = false;
                    log.error('ses.sendEmail:error: ', err);
                    repository.saveLog(uuidV1(), addressParam, processedSubject, isSuccess, err);
                    return reject(err);
                }

                log.info('ses.sendEmail:success: ', result);
                repository.saveLog(uuidV1(), addressParam, processedSubject, isSuccess, err);
                return resolve(result);
            });
        });
    }
}

module.exports = new LogicEmail();
