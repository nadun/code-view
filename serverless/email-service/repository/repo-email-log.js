const log = require('../util/logging');
const dynamoose = require('./connector-dynamodb');
const schema = require('./schema-email-log');

const repository = dynamoose.model(process.env.logTableName, schema);

repository.saveLog = (emailId, addressParam, subject, isSuccess, response) => new Promise((resolve, reject) => {
    const params = {
        EmailId: emailId,
        AddressOBj: addressParam,
        Subject: subject,
        IsSuccess: isSuccess,
        Response: response,
        CreatedAt: new Date().toISOString()
    };
    repository.create(params, (err, result) => {
        if (err) {
            log.error('repo.savelog:error: ', err);
            return reject(err);
        }
        log.info('repo.savelog:success: ', result);
        return resolve(result);
    });
});

module.exports = repository;
