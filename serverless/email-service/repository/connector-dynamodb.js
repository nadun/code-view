const dynamoose = require('dynamoose');
const https = require('https');
const sslAgent = new https.Agent({
    keepAlive: true,
    rejectUnauthorized: true,
    maxSockets: 50
})

sslAgent.setMaxListeners(0);
const dynamooseConfigString = process.env.dynamoose || '{"create":true,"update":true}';
const dynamooseConfig = JSON.parse(dynamooseConfigString);

dynamoose.AWS.config.update({
    region: process.env.region || 'us-west-2',
    httpOptions: {
        agent: sslAgent
    }
});

dynamoose.setDefaults({create: dynamooseConfig.create, update: dynamooseConfig.update,
    waitForActive: false});

module.exports = dynamoose;
