const dynamoose = require('./connector-dynamodb');

const { Schema } = dynamoose;

const config = JSON.parse(process.env.dynamoose);

const EmailLogSchema = new Schema(
    {
        EmailId: {
            type: String,
            trim: true,
            hashKey: true
        },
        AddressOBJ: {
            type: Object
        },
        To: {
            type: Array
        },
        Cc: {
            type: Array,
            default: []
        },
        Bcc: {
            type: Array,
            default: []
        },
        Subject: {
            type: String
        },
        IsSuccess: {
            type: Boolean,
            default: false,
        },
        Response: {
            type: Object,
            default: null
        },
        CreatedAt: {
            type: String,
            rangeKey: true
        }
    },
    {
        throughput: {
            read: config.throughput.EmailLog.read,
            write: config.throughput.EmailLog.write
        }
    }
);

module.exports = EmailLogSchema;
