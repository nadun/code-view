const dynamoose = require('./connector-dynamodb');

const { Schema } = dynamoose;

const config = JSON.parse(process.env.dynamoose);

var Case = new Schema({
    CaseId: {
        type: String,
        hashKey: true
    },
    DocumentName: {
        type: String
    },
    PatientId: {
        type: String
    },
    PatientName: {
        type: String
    },
    CaseType: {
        type: Number
    },
    CaseName: {
        type: String
    },
    CaseTimelineType: {
        type: Number
    },
    CaseStatus: {
        type: Number
    },
    FileType: {
        type: String
    },
    ContentType: {
        type: String
    },
    Caption: {
        type: String
    }, 
    Description: {
        type: String
    },
    ReportData: {
        type: String
    },
    AppVersion: {
        type: String
    },
    CreatedBy: {
        type: String,
        index: {
            global: true,
            name: 'User-Created-Index',
            throughput: {
                read: 2,
                write: 2
            } // read and write are both 1
        }
    },
    UpdatedBy: {
        type: String,
        index: {
            global: true,
            name: 'User-Updated-Index',
            throughput: {
                read: 2,
                write: 2
            } // read and write are both 1
        }
    }
}, {
    useDocumentTypes: true,
    throughput: {
        read: 2,
        write: 2
    },
    timestamps: {
        createdAt: 'CreatedAt',
        updatedAt: 'UpdatedAt'
    }
});

module.exports = Case;
