const dynamoose = require('./connector-dynamodb');
const schema = require('./schema');

const repository = dynamoose.model('Case', schema);

repository.newFunction = () => 'new functions';

module.exports = repository;
