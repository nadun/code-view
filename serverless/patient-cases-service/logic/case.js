/* eslint class-methods-use-this:[off] */
const log = require('../util/logging');
const caseRepo = require('../repository/repo')
const uuidv4 = require("uuid/v4")

class Case {
    
    create(document_name, pattient_id, patient_name, case_type,
        case_name, case_timeline_type, case_status, file_type, content_type, caption, 
        description, report_data, app_version, user_id) {
        return new Promise((resolve, reject) => {
            let newCase = new caseRepo({
                CaseId: uuidv4(),
                DocumentName: document_name,
                PatientId: pattient_id,
                PatientName: patient_name,
                CaseType: case_type,
                CaseName: case_name,
                CaseTimelineType: case_timeline_type,
                CaseStatus: case_status,
                FileType: file_type,
                ContentType: content_type,
                Caption: caption, 
                Description: description,
                ReportData: report_data,
                AppVersion: app_version,
                CreatedBy: user_id,
            })

            newCase.save(err => {
                if (err) {
                    log.error(err)
                    reject(err)
                }

                resolve("case created successful")
            })
        })
    }

    edit(case_id, document_name, pattient_id, patient_name, case_type,
        case_name, case_timeline_type, case_status, file_type, content_type, caption, 
        description, report_data, app_version, user_id) {

        let _self = this;

        return new Promise(async (resolve, reject) => {

            try {

                let res = await _self.getCase(case_id, user_id)

                if (res === "cannot find the case") {
                    log.error(res)
                    throw new Error(res)
                }

                let updateObj = {}

                if (document_name) {
                    updateObj["DocumentName"] = document_name;
                }

                if (patient_name) {
                    updateObj["PatientName"] = patient_name;
                }

                if (case_type) {
                    updateObj["CaseType"] = case_type;
                }

                if (case_name) {
                    updateObj["CaseName"] = case_name;
                }

                if (case_timeline_type) {
                    updateObj["CaseTimelineType"] = case_timeline_type;
                }

                if (case_status) {
                    updateObj["CaseStatus"] = case_status;
                }

                if (file_type) {
                    updateObj["FileType"] = file_type;
                }

                if (content_type) {
                    updateObj["ContentType"] = content_type;
                }

                if (caption) {
                    updateObj["Caption"] = caption;
                }

                if (description) {
                    updateObj["Description"] = description;
                }

                if (report_data) {
                    updateObj["ReportData"] = report_data;
                }

                if (app_version) {
                    updateObj["AppVersion"] = app_version;
                }

                if (user_id) {
                    updateObj["UpdatedBy"] = user_id;
                }

                caseRepo.update({CaseId: case_id}, updateObj, function (err) {
                    if (err) {
                        return reject(err)
                    }

                    resolve("update success")
                });

            } catch (err) {
                log.error(err)
                return reject(err)
            }

        })
    }

    getAll(limit, last_key, patient_id, user_id) {
        limit = limit || 100;
        last_key = last_key || null
        return new Promise((resolve, reject) => {

            let query = {};

            if (limit) {

                if (last_key != null) {
                    query = caseRepo.scan({PatientId: patient_id, CreatedBy: user_id}).startAt(last_key).limit(limit)
                } else {
                    query = caseRepo.scan({PatientId: patient_id, CreatedBy: user_id}).limit(limit)
                }
            }

            query.exec((err, result) => {
                if (err) {
                    return reject(err)
                }

                let lastKey = result.lastKey || null
                resolve({data:
                     result, last_key: lastKey})
            })
        })
    }

    getCase(case_id, user_id) {
        
        return new Promise((resolve, reject) => {
            caseRepo.get({CaseId: case_id}, (err, usercase) => {
                if (err) {
                    log.error(err)
                    return reject(reject)
                }

                if (usercase) {

                    if (usercase.CreatedBy == user_id || usercase.UpdatedBy == usercase) {
                        return resolve(usercase)
                    }
                }

                log.error("cannot find the case")
                return reject("cannot find the case")

            })
        })
    }
}

module.exports = new Case()
