
service: patient-case-service

provider:
  name: aws
  runtime: nodejs10.x
  stage: ${opt:stage, 'dev'}
  environment: ${file(./config.js):${self:provider.stage}}
  region: ${opt:region, '${self:provider.environment.region}'}
  deploymentBucket: ${opt:bucket, '${self:provider.environment.deploymentBucket}'}
  memorySize: ${opt:memory, '1024'}
  timeout: ${opt:timeout, '60'}
  tracing: true

  iamRoleStatements:
    - Effect: "Allow"
      Action:
        - "dynamodb:*"
      Resource: "arn:aws:dynamodb:${self:provider.region}:*:table/*"
    - Effect: "Allow"
      Action:
        - "lambda:InvokeAsync"
        - "lambda:InvokeFunction"
      Resource: "*"
    - Effect: "Allow" # xray permissions (required) 
      Action:
        - "xray:PutTraceSegments"
        - "xray:PutTelemetryRecords"
      Resource:
        - "*"

functions:
  create:
    handler: handler/index.create
    events:
      - http:
          path: create
          method: post
          cors: true
          authorizer:
            arn: ${self:provider.environment.authorizerArn}
  
  edit:
    handler: handler/index.edit
    events:
      - http:
          path: edit
          method: put
          cors: true
          authorizer:
            arn: ${self:provider.environment.authorizerArn}

  getAll:
    handler: handler/index.getAll
    events:
      - http:
          path: getAll
          method: post
          cors: true   
          authorizer:
            arn: ${self:provider.environment.authorizerArn}
  
  get:
    handler: handler/index.get
    events:
      - http:
          path: get
          method: post
          cors: true 
          authorizer:
            arn: ${self:provider.environment.authorizerArn}


resources:
  Resources:
    ApiGatewayRestApi:
      Type: AWS::ApiGateway::RestApi
      Properties:
        Name: ${self:service}-${self:provider.stage}
    #### Gateway Response INIT
    GatewayResponseDefault400:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: DEFAULT_4XX
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":\"4XX\",\"code\":\"custom-4XX-generic\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseDefault500:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: DEFAULT_5XX
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":\"5XX\",\"code\":\"custom-5XX-generic\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseAccessDenied:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: ACCESS_DENIED
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":403,\"code\":\"custom-403-access-denied\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseApiConfigurationError:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: API_CONFIGURATION_ERROR
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":500,\"code\":\"custom-500-api-configuration-error\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseAuthorizerConfigurationError:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: AUTHORIZER_CONFIGURATION_ERROR
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":500,\"code\":\"custom-500-authorizer-configuration-error\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseAuthorizerFailure:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: AUTHORIZER_FAILURE
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":500,\"code\":\"custom-500-authorizer-failure\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseBadRequestBody:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: BAD_REQUEST_BODY
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":400,\"code\":\"custom-400-bad-request-body\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseBadRequestParameters:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: BAD_REQUEST_PARAMETERS
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":400,\"code\":\"custom-400-bad-request-parameters\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseExpiredToken:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: EXPIRED_TOKEN
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":403,\"code\":\"custom-403-expired-token\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseIntegrationFailure:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: INTEGRATION_FAILURE
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":504,\"code\":\"custom-504-integration-failure\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseIntegrationTimeout:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: INTEGRATION_TIMEOUT
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":504,\"code\":\"custom-504-integration-timeout\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseInvalidApiKey:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: INVALID_API_KEY
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":403,\"code\":\"custom-403-invalid-api-key\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseInvalidSignature:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: INVALID_SIGNATURE
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":403,\"code\":\"custom-403-invalid-signature\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseMissingAuthenticationToken:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: MISSING_AUTHENTICATION_TOKEN
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":403,\"code\":\"custom-403-missing-authentication-token\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseRequestTooLarge:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: REQUEST_TOO_LARGE
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":413,\"code\":\"custom-413-request-too-large\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseResourceNotFound:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: RESOURCE_NOT_FOUND
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":404,\"code\":\"custom-404-resource-not-found\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
    GatewayResponseUnauthorized:
      Type: 'AWS::ApiGateway::GatewayResponse'
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        RestApiId: 
          Ref: 'ApiGatewayRestApi'
        ResponseType: UNAUTHORIZED
        ResponseTemplates:
          application/json: "{\"error\":{\"statusCode\":401,\"code\":\"custom-401-unauthorized\",\"message\":$context.error.messageString},\"requestId\":\"$context.requestId\"}"
          
package:
  exclude:
    - .serverless/**

custom:
  scripts:
    hooks:
      'package:initialize': node -e "var cd = require('check-dependencies').sync({});if(cd.error.length>0) throw new Error(JSON.stringify(cd.error))"
  prune:
    automatic: true
    number: 5

plugins:
  - serverless-offline
  - serverless-plugin-tracing
  - serverless-prune-plugin
  - serverless-plugin-scripts
