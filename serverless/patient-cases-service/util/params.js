const log = require('./logging');

const isString = obj => typeof obj === 'string' || obj instanceof String;

const getBody = (event) => {
    if (event) {
        if (Object.prototype.hasOwnProperty.call(event, 'body') && !event.body) {
            log.info('Input: ', {});
            return {};
        }

        let params = event.body || event;

        try {
            params = isString(params) ? JSON.parse(params) : params;
        } catch (e) {
            params = {};
        }

        log.info('Input: ', params);

        return params;
    }

    log.info('Input: ', {});

    return {};
};


const getQuery = (event) => {
    if (event) {
        if (Object.prototype.hasOwnProperty.call(event, 'queryStringParameters') && !event.queryStringParameters) {
            log.info('Input: ', {});
            return {};
        }

        const params = event.queryStringParameters || event;

        log.info('Input: ', params);

        return params;
    }

    log.info('Input: ', {});

    return {};
};

module.exports = {
    getBody,
    getQuery,
    isString
};
