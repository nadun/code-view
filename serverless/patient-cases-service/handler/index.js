const caseLogic = require('../logic/case');
const paramsUtils = require('../util/params');
const responseUtils = require('../util/response');
const validation = require('../validation');
const log = require('../util/logging');

const create = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false;
    let user_id; 
    
    try{
        const body = paramsUtils.getBody(event);

        if (isHttp) {
            let user = event.requestContext.authorizer.claims;
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
            }
            user_id = user.sub;
        } else {
            user_id = body.user_id;
        }
        // user_id = "fd0d7d92-b30c-4238-bceb-6256cd1ef1b2";

        log.info(`body: ${JSON.stringify(body)})`);

        const {
            document_name,
            patient_id,
            patient_name,
            case_type,
            case_name,
            case_timeline_type,
            case_status,
            file_type,
            content_type,
            caption, 
            description,
            report_data,
            app_version
        } = body;
        
        //validating user attributes
        await validation.validate(body, '/CaseCreate');

        //creating patient case 
        let result = await caseLogic.create(document_name, patient_id, patient_name, case_type,
            case_name, case_timeline_type, case_status, file_type, content_type, caption, 
            description, report_data, app_version, user_id);


        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        
        return response
    }catch(err){
        log.error(err)
        const response = responseUtils.errorResponse(400, err.message, isHttp);
    
        return response
    }
}

const edit = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false;
    let user_id;
    const body = paramsUtils.getBody(event);

    if (isHttp) {
        let user = event.requestContext.authorizer.claims;
        if (paramsUtils.isString(user)) {
            user = JSON.parse(user);
        }
        user_id = user.sub;
    } else {
        user_id = body.user_id;
    }
    // user_id = "fd0d7d92-b30c-4238-bceb-6256cd1ef1b2";

    try{

        log.info(`body: ${JSON.stringify(body)})`);

        const {
            case_id,
            document_name,
            pattient_id,
            patient_name,
            case_type,
            case_name,
            case_timeline_type,
            case_status,
            file_type,
            content_type,
            caption, 
            description,
            report_data,
            app_version,
        } = body;
        
        //validating user attributes
        await validation.validate(body, '/CaseUpdate')

        //editing case
        let result = await caseLogic.edit(case_id, document_name, pattient_id, patient_name, case_type,
            case_name, case_timeline_type, case_status, file_type, content_type, caption, 
            description, report_data, app_version, user_id)

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);
    
        return response
    }
    
}

const get = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false;

    try{
        
        let user_id;
        const body = paramsUtils.getBody(event);

        // if (isHttp) {
        //     let user = event.requestContext.authorizer.claims;
        //     if (paramsUtils.isString(user)) {
        //         user = JSON.parse(user);
        //     }
        //     user_id = user.sub;
        // } else {
        //     user_id = body.user_id;
        // }
        
        user_id = "fd0d7d92-b30c-4238-bceb-6256cd1ef1b2";

        log.info(`body: ${JSON.stringify(body)})`);

        const {
            case_id
        } = body;

        let result = await caseLogic.getCase(case_id, user_id)
        if (!result) {
            result = {}
        }

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);
    
        return response
    }
}

const getAll = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false;
    try{
        
        const body = paramsUtils.getBody(event);
        let user_id;

        if (isHttp) {
            let user = event.requestContext.authorizer.claims;
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
                user_id = user.sub
            } else {
                user_id = user.sub
            }
        } else {
            user_id = body.user_id
        }
        // user_id = "fd0d7d92-b30c-4238-bceb-6256cd1ef1b2"

        const { limit, last_key, patient_id} = body

        log.info(`body: ${JSON.stringify(body)})`, user_id)

        //get cases by patient
        let result = await caseLogic.getAll(limit, last_key, patient_id, user_id)
        if (!result) {
            result = {}
        }

        const response = responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        },isHttp);

        return response

    }catch(err){
        const response = responseUtils.errorResponse(400, err.message, isHttp);
    
        return response
    }
}


module.exports = {
    create,
    edit,
    get,
    getAll
};