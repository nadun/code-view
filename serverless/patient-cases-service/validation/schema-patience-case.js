module.exports = {
    CaseCreate: {
        id: '/CaseCreate',
        type: 'object',
        properties: {
            document_name: {type: 'string'},
            patient_id: {type: 'string'},
            patient_name: {type: 'string'},
            case_type: {type: 'Number'},
            case_name: {type: 'string'},
            case_timeline_type: {type: 'Number'},
            case_status: {type: 'Number'},
            file_type: {type: 'string'},
            content_type: {type: 'string'},
            caption: {type: 'string'}, 
            description: {type: 'string'},
            report_data: {type: 'string'},
            app_version: {type: 'string'},
            user_id: {type: 'string'}
        },
        required: ['patient_id', 'document_name']
    },
    CaseUpdate: {
        id: '/CaseUpdate',
        type: 'object',
        properties: {
            document_name: {type: 'string'},
            pattient_id: {type: 'string'},
            patient_name: {type: 'string'},
            case_type: {type: 'Number'},
            case_name: {type: 'string'},
            case_timeline_type: {type: 'Number'},
            case_status: {type: 'Number'},
            file_type: {type: 'string'},
            content_type: {type: 'string'},
            caption: {type: 'string'}, 
            description: {type: 'string'},
            report_data: {type: 'string'},
            app_version: {type: 'string'},
            user_id: {type: 'string'}
        },
        required: ['pattient_id', 'document_name']
    }
    
};
