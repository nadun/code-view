const paramsUtils = require('../util/params');
const responseUtils = require('../util/response');
const validation = require('../validation');
const log = require('../util/logging');

const executeHandlerFunction = (handlerFunction, body, callback) => {
    handlerFunction(body, (err, result) => {
        if (err) {
            const response = responseUtils.errorResponse(400, err);
            log.error(`Handler ERROR: ${JSON.stringify(response)}`);
            callback(response);

            return;
        }

        log.info(`Handler success, result: ${JSON.stringify(result)}`);

        const response = responseUtils.successResponse(result);

        callback(null, response);
    });
};

const handleRequest = (validationID, handlerFunction, event, callback) => {
    const body = paramsUtils.getBody(event);   
    validation
        .validate(body, validationID)
        .then((validationResult) => {
            log.info(`validation result: ${JSON.stringify(validationResult)}`);
            executeHandlerFunction(handlerFunction, body, callback);
        })
        .catch((err) => {
            console.log(err);
            const errResponse = responseUtils.errorResponse(400, err);
            log.error(`${validationID} Validation error: ${errResponse} (${typeof err})`);
            callback(errResponse);
        });
};

module.exports = {
    handleRequest
};
