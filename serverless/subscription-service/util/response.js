const log = require('./logging');


const successResponseHttpExceptDataKey = (data) => {
    log.info('Output: ', data);

    return {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Origin": "*"
        },
        body:  JSON.stringify(data) || null
    };
};

const successResponseHttp = (data) => {
    log.info('Output: ', data);

    return {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify(data || null)
    };
};

const errorResponseHttp = (code, err) => {
    log.error('Error: ', err);

    return {
        statusCode: code || 400,
        headers: {
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify({
            err
        })
    };
};

const successResponse = (data, isHttp) => {
    log.info('Output: ', data);

    if(isHttp){
        return successResponseHttp(data)
    }else{
        return {
            statusCode: 200,
            headers:{
                "Access-Control-Allow-Origin": "*"
            },
            body: {
                data: data || null
            }
        };
    }

};

const successResponseExceptDataKey = (data, isHttp) => {
    log.info('Output: ', data);

    if(isHttp){
        return successResponseHttpExceptDataKey(data)
    }else{
        return {
            statusCode: 200,
            headers:{
                "Access-Control-Allow-Origin": "*"
            },
            body: data || null

        };
    }

};

const errorResponse = (code, err, isHttp) => {
    log.error('Error: ', err);

    if(isHttp){
        return errorResponseHttp(code, err)
    }else{
        return {
            statusCode: code || 400,
            headers:{
                "Access-Control-Allow-Origin": "*"
            },
            body: {
                err: err || null
            }
        };
    }
};

const sendError = (message, callback) => {
    // const err = {
    //     error_code: 'service_unavailable',
    //     message
    // };

    callback(message);
};

module.exports = {
    successResponseHttp,
    errorResponseHttp,
    successResponse,
    errorResponse,
    sendError,
    successResponseExceptDataKey
};
