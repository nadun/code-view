const config = require('../config').dev();

const keys = Object.keys(config);
for (let i = 0; i < keys.length; i += 1) {
    const key = keys[i];
    process.env[key] = config[key];
}
