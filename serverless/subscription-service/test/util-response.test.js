const response = require('../util/response');
const logUtils = require('../util/logging');

const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

describe('util/response', () => {
    let info;
    let error;

    describe('successResponse', () => {
        beforeEach(() => {
            // Skip any log output
            info = sinon.stub(logUtils, 'info');
            error = sinon.stub(logUtils, 'error');
        });

        afterEach(() => {
            info.restore();
            error.restore();
        });

        it('Should return success response.', () => {
            expect(response.successResponse({ valA: 1, valB: 2 })).to.deep.equal({
                statusCode: 200,
                body: JSON.stringify({
                    data: { valA: 1, valB: 2 }
                })
            });
        });

        it('Should return success response, with null data if nothing is provided.', () => {
            expect(response.successResponse()).to.deep.equal({
                statusCode: 200,
                body: JSON.stringify({
                    data: null
                })
            });
        });
    });

    describe('errorResponse', () => {
        beforeEach(() => {
            // Skip any log output
            info = sinon.stub(logUtils, 'info');
            error = sinon.stub(logUtils, 'error');
        });

        afterEach(() => {
            info.restore();
            error.restore();
        });

        it('Should return fail response.', () => {
            expect(response.errorResponse(123, { valA: 1, valB: 2 })).to.deep.equal({
                statusCode: 123,
                body: JSON.stringify({
                    err: { valA: 1, valB: 2 }
                })
            });
        });
    });
});

