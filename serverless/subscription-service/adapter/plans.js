const lambda = require("../util/lambda")
const log = require("../util/logging")

const getPlan = async (plan_id) => {

    console.log(plan_id)
    try{
        let user = lambda.callLambdaPromise(`subscription-plans-service-${process.env.stage}-get`, {id: plan_id})
        return user
    }
    catch(err){
        log.error(err)
        throw new Error(err)
    }

}



module.exports = {
    getPlan,
}
