const lambda = require("../util/lambda")
const log = require("../util/logging")

const getUser = async (user_id) => {

    try{
        let user = lambda.callLambdaPromise(`users-service-${process.env.stage}-getUserById`, {user_id: user_id})
        return user
    }
    catch(err){
        log.error(err)
        throw new Error(err)
    }

}


const updateUser = async (user_id,customer_id) => {

    try{
        let user = lambda.callLambdaPromise(`users-service-${process.env.stage}-updateCustomerId`, {user_id: user_id ,customer_id:customer_id})
        return user
    }
    catch(err){
        log.error(err)
        throw new Error(err)
    }

}

module.exports = {
    getUser,
    updateUser,
}
