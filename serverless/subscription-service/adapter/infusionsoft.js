const log = require('../util/logging');
const lambda = require('../util/lambda');

const update = async (id, plan_started_date, expiration_date, subscription_plan) => {
    return await lambda.callLambdaAsyncPromise(`infusion-soft-service-${process.env.stage}-publishToSns`, {
        message_type: 'INFUSIONSOFT_CONTACT',
        message: {
            id: id,
            plan_started_date: plan_started_date,
            expiration_date: expiration_date,
            subscription_plan: subscription_plan,
        }
    })


}

module.exports = {
    update
}


