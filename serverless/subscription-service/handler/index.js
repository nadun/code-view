const logic = require('../logic/subscription');
const paramsUtils = require('../util/params');
const responseUtils = require('../util/response');
const validation = require('../validation');
const log = require('../util/logging');
const request = require('../util/request');
const _ = require('lodash');
const moment = require('moment');
const infusionAdapter = require('../adapter/infusionsoft');


const stripeWebhookHandler = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    const body = paramsUtils.getBody(event);


    let {stripeToken, object, type, data} = body;
    let {status, subscription, billing_reason, period_end} = data.object;


    try {

        if (billing_reason == 'subscription_cycle' && status == 'paid') {

            subscriptionData = await logic.getUserSubscriptionById(subscription);

            let {subscriptionStatus, subscriptionId} = subscriptionData;
            let data = {
                ended_at: period_end,
                status: subscriptionStatus,
                id: subscriptionId
            };

            subscription = await logic.updateUserSubscription(data, subscriptionData.userId, subscriptionData.id);

        }


        let response = responseUtils.successResponseHttp({
            subscription,
            development: process.env.development,
            region: process.env.region
        });

        return response;

    }
    catch (err) {
        return responseUtils.errorResponseHttp(400, err.message, isHttp);
    }
};

const createSubscription = async (event, context, callback) => {

    const isHttp = (event.httpMethod) ? true : false
    const body = paramsUtils.getBody(event);

    let {stripeToken, plan, email, subscription_plan_id } = body;

    try {

        if (isHttp) {
            let user = event.requestContext.authorizer.claims;
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
            }
            userId = user.sub;
        } else {
            userId = body.user_id;
        }

        // let userId = 'e85bda07-8dee-4a67-a89e-f4f9367a8cf6';

        let user = await logic.getUserdetails(userId);

        let subscription;
        let result = await logic.getMySubscriptions(userId);



        let planDetails = await logic.getPlan(subscription_plan_id);
        let subscription_plan = planDetails.title;

        let isNewPlan=true;

        for(let x in result.data){

            let subs =result.data[x];

            if(subs && subs.subscriptionPlanId){

                let CurrentPlanDetails = await logic.getPlan(subs.subscriptionPlanId);

                if(subs.subscriptionStatus=='active'  ){

                    subscription = await logic.getSubscriptionDetailsFromStripe(subs.subscriptionId);

                    if(CurrentPlanDetails.type==planDetails.type){

                        isNewPlan=false;

                        if( subs.planId != plan ){

                            subscription = await logic.getSubscriptionDetailsFromStripe(subs.subscriptionId);

                            newSubscription =  await logic.changePlan(subs.subscriptionId, plan , subscription);


                            let data ={
                                status: 'canceled',
                                ended_at: subs.endDate,
                                id:subs.id
                            }

                            await logic.updateUserSubscription(data, userId, subs.id);

                            newSubscription.subscriptionType = planDetails.type;

                            await logic.saveUserSubscription(newSubscription, userId, subscription_plan_id);

                            let  plan_started_date = moment(newSubscription.current_period_start*1000).format("YYYY-MM-DD");
                            let  current_period_end = moment(newSubscription.current_period_end*1000).format("YYYY-MM-DD");

                            infusionAdapter.update(user.InfusionsoftId, plan_started_date, current_period_end, subscription_plan);

                        }

                        //if send same plan again
                        if( subs.planId == plan  ){
                            subscription = await logic.getSubscriptionDetailsFromStripe(subs.subscriptionId);
                        }

                        if( subscription_plan_id!= subs.subscriptionPlanId && CurrentPlanDetails.type==planDetails.type){

                            let data ={
                                status: 'canceled',
                                ended_at: subs.endDate,
                                id: subs.id
                            }


                            await logic.updateUserSubscription(data, userId, subs.id);

                        }


                    }


                }
            }

        }

        if(isNewPlan){
            if (user.CustomerId) {
                subscription = await logic.subscribeToPlan(user.CustomerId, plan);
            }
            else {
                let customer = await logic.createCustomer(email, stripeToken);

                await logic.updateUser(userId, customer.id);

                subscription = await logic.subscribeToPlan(customer.id, plan);

            }

            subscription.subscriptionType = planDetails.type;

            // update infusion
            let  plan_started_date = moment(subscription.current_period_start*1000).format("YYYY-MM-DD");
            let  current_period_end = moment(subscription.current_period_end*1000).format("YYYY-MM-DD");

            infusionAdapter.update(user.InfusionsoftId, plan_started_date,current_period_end, subscription_plan);

            subscription = await logic.saveUserSubscription(subscription, userId, subscription_plan_id);
            delete subscription.response_data;
        }



        let response = responseUtils.successResponseHttp({
            subscription,
            development: process.env.development,
            region: process.env.region
        });

        return response;

    }
    catch (err) {
        return responseUtils.errorResponseHttp(400, err.message, isHttp);
    }


};

const deleteSubscription = async (event, context, callback) => {

    const isHttp = (event.httpMethod) ? true : false
    const body = paramsUtils.getBody(event);

    let userId;


    let {subscriptionId} = body;


    try {


        if (isHttp) {
            let user = event.requestContext.authorizer.claims;
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
            }
            userId = user.sub;
        } else {
            userId = body.user_id;
        }


        userSubscription = await logic.getUserSubscriptionById(subscriptionId);


        let subscription = await logic.cancelSubscription(userSubscription.subscriptionId);

        let user = await logic.getUserdetails(userId);

        infusionAdapter.update(user.InfusionsoftId, '', '','');


        subscription.ended_at = subscription.current_period_end;
        subscription = await logic.updateUserSubscription(subscription, userId, userSubscription.id);

        let response = responseUtils.successResponseHttp({
            subscription,
            development: process.env.development,
            region: process.env.region
        });

        return response;

    }
    catch (err) {
        return responseUtils.errorResponseHttp(400, err.message, isHttp);
    }


};

const getCards = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    const body = paramsUtils.getBody(event);

    try {

        if (isHttp) {
            let user = event.requestContext.authorizer.claims;
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
            }
            userId = user.sub;
        } else {
            userId = body.user_id;
        }


        let user = await logic.getUserdetails(userId);


        if (!user.CustomerId) {
            result = {
                "object": "list",
                "data": [],
                "has_more": false,
                "url": ""
            }
        }
        else {
            result = await logic.getCards(user.CustomerId);

        }


        const response = await responseUtils.successResponseExceptDataKey({
            result,
            development: process.env.development,
            region: process.env.region
        }, isHttp);

        return response
    }
    catch (err) {
        return responseUtils.errorResponseHttp(400, err.message, isHttp);

    }

};

const getMySubscriptions = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    const body = paramsUtils.getBody(event);

    try {

        if (isHttp) {
            let user = event.requestContext.authorizer.claims;
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
            }
            userId = user.sub;
        } else {
            userId = body.user_id;
        }

        let { limit, last_key} = body

        let result = await logic.getMySubscriptions(userId, limit, last_key);

        for(let x in result.data){

           if(result.data[x] && result.data[x].subscriptionPlanId) {
               plan = await logic.getPlan(result.data[x].subscriptionPlanId)

               let temp = { planDetails : plan };

               let obj = {...temp, ...result.data[x]}

               obj.endDate = moment(obj.endDate*1000).format("DD-MM-YYYY");
               obj.startDate = moment(obj.startDate*1000).format("DD-MM-YYYY");

               result.data[x]=obj;
           }

           else {
               let temp = { planDetails : [] };

               let obj = {...temp, ...result.data[x]}

               result.data[x]= obj;
           }

        }

        const response = await responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        }, isHttp);

        return response
    }
    catch (err) {
        return responseUtils.errorResponseHttp(400, err.message, isHttp);

    }

};

const addCard = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    const body = paramsUtils.getBody(event);

    try {

        if (isHttp) {
            let user = event.requestContext.authorizer.claims;
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
            }
            userId = user.sub;
        } else {
            userId = body.user_id;
        }


        let user = await logic.getUserdetails(userId);

        let {stripeToken} = body

        if (!user.CustomerId) {

            throw new Error('Cannot find the customer')
        }
        else {
            result = await logic.addCard(user.CustomerId, stripeToken);

        }


        const response = await responseUtils.successResponseExceptDataKey({
            result,
            development: process.env.development,
            region: process.env.region
        }, isHttp);

        return response
    }
    catch (err) {
        return responseUtils.errorResponseHttp(400, err.message, isHttp);

    }

};

const removeCard = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    const body = paramsUtils.getBody(event);

    try {


        if (isHttp) {
            let user = event.requestContext.authorizer.claims;
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
            }
            userId = user.sub;
        } else {
            userId = body.user_id;
        }

        let user = await logic.getUserdetails(userId);

        let result = {}


        if (!user.CustomerId) {
            throw new Error('Cannot find the customer')
        }
        else {
            let {card_id} = body

            let cards = await logic.getCards(user.CustomerId);

            if (cards.data.length > 1) {
                result = await logic.removeCard(user.CustomerId, card_id);
            }
            else {
                throw new Error('There should be at least one Credit Card');
            }

        }


        const response = await responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        }, isHttp);

        return response
    }
    catch (err) {
        return responseUtils.errorResponseHttp(400, err.message, isHttp);

    }

};

const changeDefaultCard = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    const body = paramsUtils.getBody(event);

    try {


        if (isHttp) {
            let user = event.requestContext.authorizer.claims;
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
            }
            userId = user.sub;
        } else {
            userId = body.user_id;
        }

        let user = await logic.getUserdetails(userId);
        let result = {}

        if (!user.CustomerId) {
            throw new Error('Cannot find the customer')
        }
        else {
            let {card_id} = body

            let cards = await logic.getCards(user.CustomerId);

            if (cards.data.length > 0) {

                result = await logic.changeDefaultCard(user.CustomerId, card_id);
            }
            else {
                throw new Error('There should be at least one Credit Card');
            }

        }

        const response = await responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        }, isHttp);

        return response
    }
    catch (err) {
        return responseUtils.errorResponseHttp(400, err.message, isHttp);

    }

}


const getInvoices = async (event, context, callback) => {
    const isHttp = (event.httpMethod) ? true : false
    const body = paramsUtils.getBody(event);

    try {

        if (isHttp) {
            let user = event.requestContext.authorizer.claims;
            if (paramsUtils.isString(user)) {
                user = JSON.parse(user);
            }
            userId = user.sub;
        } else {
            userId = body.user_id;
        }

        let user = await logic.getUserdetails(userId);


        if (!user.CustomerId) {
            throw new Error('Cannot find the customer')
        }

        let result = await logic.getInvoices(user.CustomerId);

        const response = await responseUtils.successResponse({
            result,
            development: process.env.development,
            region: process.env.region
        }, isHttp);

        return response
    }
    catch (err) {
        return responseUtils.errorResponseHttp(400, err.message, isHttp);

    }

}


module.exports = {
    createSubscription,
    getMySubscriptions,
    stripeWebhookHandler,
    deleteSubscription,
    getCards,
    addCard,
    removeCard,
    changeDefaultCard,
    getInvoices
};
