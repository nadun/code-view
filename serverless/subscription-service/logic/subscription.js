/* eslint class-methods-use-this:[off] */
const log = require('../util/logging');
const {Subscription} = require('../repository/repo');
const userAdapter = require('../adapter/user');
const planAdapter = require('../adapter/plans');

const stripe = require('stripe')(process.env.stripeKey);
const uuidv4 = require('uuid/v4');

class Test {
    subscribeToPlan(customer_id, plan) {

        return new Promise(function (resolve, reject) {

            stripe.subscriptions.create(
                {
                    customer: customer_id,
                    items: [{plan: plan}],
                },
                function (err, subscription) {

                    if (err) {
                        reject(err)
                    }

                    resolve(subscription)

                }
            );
        });


    }

    createCustomer(email, token) {

        return new Promise(function (resolve, reject) {

            stripe.customers.create(
                {
                    email: email,
                    source: token
                },
                function (err, customer) {
                    if (err) {
                        reject(err)
                    }
                    else {

                        resolve(customer)
                    }

                }
            );

        });

    }

    async getUserdetails(user_id) {
        let user = await userAdapter.getUser(user_id)
        log.info("--user--", user.data.result)
        return user.data.result
    }

    async updateUser(user_id, customer_id) {
        let user = await userAdapter.updateUser(user_id, customer_id)
        log.info("--user--", user.data.result)
        return user.data.result
    }

    async getPlan(id) {
        let user = await planAdapter.getPlan(id)
        log.info("--plan--", user.data.result)

        return user.data.result
    }

    async cancelSubscription(subscription_id) {

        return new Promise(function (resolve, reject) {
            stripe.subscriptions.del(
                subscription_id,
                function (err, confirmation) {
                    if (err) {
                        reject(err)
                    }
                    resolve(confirmation)
                }
            );


        });

    }

    saveUserSubscription(data, user_id, subscription_plan_id) {

        let {
            id,
            current_period_end,
            current_period_start,
            customer,
            discount,
            status,
            plan,
            subscriptionType
        } = data


        return new Promise((resolve, reject) => {
            let subscription = new Subscription({
                id: uuidv4(),
                customerId: customer,
                endDate: current_period_end,
                startDate: current_period_start,
                userId: user_id,
                planId: plan.id,
                paidPlanAmount: plan.amount,
                discount: discount,
                subscriptionStatus: status,
                subscriptionId: id,
                subscriptionPlanId: subscription_plan_id,
                subscriptionType : subscriptionType,
            })

            subscription.save((err, sub) => {
                if (err) {
                    log.error(err)
                    reject(err)
                }
                resolve(sub)
            })
        })


    }

    getUserSubscriptionById(subscriptionId) {

        return new Promise(function (resolve, reject) {

            Subscription.scan({subscriptionId: subscriptionId}, function (err, res) {
                if (err) {
                    reject(err)
                }
                if (res[0])
                    resolve(res[0])
                else
                    reject({message: 'subscription details cannot be find'})

            });
        })
    }

    async updateUserSubscription(data, UserId, uuid) {

        let {ended_at, status, id} = data;

        return new Promise(function (resolve, reject) {

            Subscription.update({UserId: UserId, subscriptionId: id, id: uuid}, {
                $PUT: {
                    subscriptionStatus: status,
                    endDate: ended_at
                }
            }, function (err) {
                if (err) {
                    return reject(err)
                }
                resolve("update success")
            });
        });

    }

    getMySubscriptions(user_id, limit, last_key) {

        limit = limit || 100;
        last_key = last_key || null

        return new Promise((resolve, reject) => {

            let query = {};

            if (limit) {

                if (last_key != null) {
                    query = Subscription.scan('userId').contains(user_id).startAt(last_key).limit(limit)
                } else {
                    query = Subscription.scan('userId').contains(user_id).limit(limit)
                }
            }

            query.exec((err, result) => {

                if (err) {
                    return reject(err)
                }


                let lastKey = result.lastKey || null
                resolve({data: result, last_key: lastKey})
            })
        })

    }

    getInvoices(customer_id) {

        return new Promise(function (resolve, reject) {

            stripe.invoices.list(
                {limit: 100, customer: customer_id},
                function (err, invoices) {
                    if (err) {
                        reject(err)
                    }
                    resolve(invoices);
                }
            );


        });

    }

    getCards(customer_id) {

        return new Promise(function (resolve, reject) {

            stripe.customers.listSources(
                customer_id,
                function (err, cards) {
                    if (err) {
                        reject(err)
                    }
                    resolve(cards);
                }
            );

        });

    }

    removeCard(customer_id, card_id) {

        return new Promise(function (resolve, reject) {
            stripe.customers.deleteSource(
                customer_id,
                card_id,
                function (err, confirmation) {
                    if (err) {
                        reject(err)
                    }
                    resolve(confirmation);
                }
            );

        });

    }

    addCard(customer_id, stripeToken) {

        return new Promise(function (resolve, reject) {

            stripe.customers.createSource(
                customer_id,
                {source: stripeToken},
                function (err, card) {
                    if (err) {
                        return reject(err)
                    }
                    return resolve(card);
                }
            );

        });

    }

    changeDefaultCard(customer_id, card_id) {
        return new Promise(function (resolve, reject) {
            stripe.customers.update(
                customer_id,
                {default_source: card_id},
                function (err, customer) {
                    if (err)
                        reject(err)
                    resolve(customer);
                }
            );
        })
    }

    changePlan(subscriptionId, newPlan, subscription) {
        return new Promise(function (resolve, reject) {

            stripe.subscriptions.update(subscriptionId, {
                cancel_at_period_end: false,
                items: [{
                    id: subscription.items.data[0].id,
                    plan: newPlan,
                }]
            }, function (err, subs) {
                if (err) {
                    return reject(err)
                }

                return resolve(subs);
            });

        })


    }

    getSubscriptionDetailsFromStripe(subscriptionId) {

        return new Promise(function (resolve, reject) {

            stripe.subscriptions.retrieve(subscriptionId, function (err, subs) {
                if (err) {
                    return reject(err)
                }
                resolve(subs)
            })

        });
    }


}

module.exports = new Test();
