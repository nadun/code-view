const dynamoose = require('./connector-dynamodb');

const { Schema } = dynamoose;

const config = JSON.parse(process.env.dynamoose);

const Subscription = new Schema(
    {
        id: {
            type: String,
            trim: true,
            hashKey: true
        },

        userId: {
            type: String,
            required: true,
            index: {
                global: true
            }
        },

        planId: {
            type: String
        },

        txnId: {
            type: String
        },

        profile_id: {
            type: String
        },

        customerId: {
            type: String
        },

        chargeId: {
            type: String
        },

        paidPlanAmount: {
            type: String
        },

        promoCodeId: {
            type: String
        },

        discount: {
            type: Number
        },

        startDate: {
            type: String
        },

        endDate: {
            type: String
        },

        subscriptionStatus: {
            type: String
        },

        subscriptionId: {
            type: String,
            required: true,
            index: {
                global: true
            }
        },
        responseData: {
            type: Object
        },
        subscriptionPlanId:{
            type: String
        },
        subscriptionType:{
            type: String
        }
    },
    {
        throughput: {
            read: config.throughput.CalculationTable.read,
            write: config.throughput.CalculationTable.write
        },

        timestamps: {
            createdAt: 'createdAt',
            updatedAt: 'updatedAt'
        },
        useDocumentClient: true
    }
);

module.exports = Subscription;
