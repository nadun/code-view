const dynamoose = require('./connector-dynamodb');
const schema = require('./schema');

const Subscription = dynamoose.model('SubscriptionDetails', schema);

module.exports = {
    Subscription
};
