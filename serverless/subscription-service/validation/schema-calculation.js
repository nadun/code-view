module.exports = {
    calculation: {
        id: '/Calculation',
        type: 'object',
        properties: {
            valA: { type: 'number' },
            valB: { type: 'number' }
        },
        required: ['valA', 'valB']
    }
};
