const { Validator } = require('jsonschema');
const fs = require('fs');
const _ = require('lodash');

class Validation {
    constructor() {
        this.v = new Validator();

        const self = this;
        const pathSchema = this._validationSchemaPath();
        const files = fs.readdirSync(pathSchema);

        // iterate over all files in validation folder
        files.forEach((file) => {
            // exclude all files that dont match the name schema-*.js
            if (!file || !/^schema-.*.js$/.test(file)) { return; }

            // reads all schemas in that object
            const schema = require(`${pathSchema}/${file}`); // eslint-disable-line
            // console.log("File: " + file);
            self.readSchemaCollection(schema);
        });
    }

    /* Reads a schema collection:
       {
            id_a: {...},
            id_b: {...},
            ...
       }
     */
    readSchemaCollection(schemaCollection) {
        const self = this;

        _.forEach(schemaCollection, (schema) => {
            // console.log("\tobj: " + schema.id);
            self.v.addSchema(schema, schema.id);
        });
    }

    validate(object, type) {
        const schema = this.v.schemas[type];

        return new Promise((resolve, reject) => {
            const validation = this.v.validate(object, schema);

            if (validation.errors.length > 0) {
                reject(validation.errors);
            } else {
                resolve(object);
            }
        });
    }

    _validationSchemaPath() {
        return `${process.cwd()}/validation`;
    }
}

module.exports = new Validation();