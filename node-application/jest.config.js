module.exports = {
  verbose: true,
  testPathIgnorePatterns: [
    '/node_modules/',
    './src/config/',
    './src/validation/',
  ],
  collectCoverage: true,
  collectCoverageFrom: [
    './src/logic/**/*.js',
    './src/util/**/*.js',
    './src/db/**/*.js',
    './src/api/**/*.js',
  ],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: -10,
    },
  },
};
