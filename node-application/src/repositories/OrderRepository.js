import _ from 'lodash';
import BaseRepository from './BaseRepository';

export default class OrderRepository extends BaseRepository {
  constructor(stricMode = false) {
    super('Order', stricMode);
  }
}