import _ from 'lodash';
import BaseRepository from './BaseRepository';

export default class FeeSettingsRepository extends BaseRepository {
  constructor(stricMode = false) {
    super('FeeSettings', stricMode);
  }
}
