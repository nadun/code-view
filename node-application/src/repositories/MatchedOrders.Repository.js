import _ from 'lodash';
import BaseRepository from './BaseRepository';

export default class MatchedOrdersRepository extends BaseRepository {
  constructor(stricMode = false) {
    super('MatchOrders', stricMode);
  }
}
