import _ from 'lodash';
import db from '../db';

export default class BaseRepository {
  constructor(domain, stictMode = true) {
    this.domain = domain;
    this.model = db[domain];
    this.strict = stictMode;
  }

  getFields() {
    return Object.keys(this.model.rawAttributes);
  }

  async findAll(options) {
    let defaultOptions = {
      where: {},
    };

    options = _.extend(defaultOptions, options);

    try {
      let result = await this.model.findAll(options);
      return Promise.resolve(result);
    } catch (e) {
      return Promise.reject(e);
    }
  }

  async list(options) {
    let defaultOptions = {
      where: {},
      include: false,
      attributes: false,
      order: false,
      paginate: {
        page: 1,
        size: 10,
      },
    };

    options = _.extend(defaultOptions, options);

    let find = {
      where: options.where,
      limit: options.paginate.size,
      offset:
        options.paginate.size *
        ((options.paginate.page > 0 ? options.paginate.page : 1) - 1),
    };

    if (options.order) {
      find.order = options.order;
    }
    if (options.include) {
      find.include = options.include;
    }
    if (options.attributes) {
      find.attributes = options.attributes;
    }

    try {
      let result = await this.model.findAndCountAll(find);
      result = {
        data: result.rows,
        pagination: {
          from:
            options.paginate.size *
            ((options.paginate.page > 0 ? options.paginate.page : 1) - 1),
          page: options.paginate.page,
          rowsPerPage: options.paginate.size,
          totalItems: result.count,
        },
      };

      if (options.order) {
        result.pagination.orderBy = options.order[0];
        result.pagination.descending =
          options.order[1] == 'desc' ? true : false;
      }

      return Promise.resolve(result);
    } catch (e) {
      console.log(e);
      return Promise.reject(e);
    }
  }

  async search(where, attributes) {
    return this.model.findOne(
      _.extend({ where }, attributes ? { attributes } : {})
    );
  }

  async read(where, attributes) {
    return this.model.findOne(
      _.extend({ where }, attributes ? { attributes } : {})
    );
  }

  async getById(id) {
    let where = { id: id };

    return this.model.findOne({ where });
  }

  async create(data) {
    return this.model.create(data);
  }

  async update(data, where) {
    try {
      let user = await this.model.update(data, { where });
      return this.read(where);
    } catch (e) {
      return Promise.reject(e);
    }
  }

  async delete(where) {
    let data = await this.model.destroy({
      where,
    });
    
    return this.read(data);
  }
}
