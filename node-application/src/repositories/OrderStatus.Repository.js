import _ from 'lodash';
import BaseRepository from './BaseRepository';

export default class OrderStatusRepository extends BaseRepository {
  constructor(stricMode = false) {
    super('OrderStatus', stricMode);
  }
}
