import db from '../db';

export async function doesExist(model, where) {
  if (!model || !db[model]) {
    return Promise.reject('INVALID_MODEL');
  }
  if (!where) {
    return Promise.reject('CONDITION_REQUIRED');
  }
  try {
    let result = await db[model].findAll({ where });
    if (result.length > 0) {
      return Promise.resolve(result.length);
    } else {
      return Promise.resolve(false);
    }
  } catch (e) {
    return Promise.resolve(false);
  }
}
