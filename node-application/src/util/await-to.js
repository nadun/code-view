//@flow
import to from 'await-to-js';
import pe from 'parse-error';

export default async (promise) => {
  let [err, res] = await to(promise);
  if (err) return [pe(err)];

  return [null, res];
};
