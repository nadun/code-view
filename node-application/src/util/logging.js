const pino = require('pino');
const config = require('../config')

const loggingConfigString ='{"enabled":true,"level":"debug"}';

const loggingConfig = (config.logging) ? config.logging : JSON.parse(loggingConfigString);

const log = pino(
  {
    name: process.env.serviceName,
    safe: true,
    levelFirst: true,
    prettyPrint: true,
  },
  process.stdout
);


if (loggingConfig.enabled === true) {
  log.level = loggingConfig.level;
} else {
  log.level = 'silent';
}

module.exports = log;
