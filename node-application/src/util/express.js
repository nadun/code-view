const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const to = require('../util/await-to');

const log = require('../util/logging');

const pino = require('express-pino-logger')({
  logger: log,
});
const moment = require('moment');
const config = require('../config');
const cors = require('cors');

const app = express();
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '../views'));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(pino);

module.exports = app;
