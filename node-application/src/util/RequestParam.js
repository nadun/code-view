export default class RequestParam {
    static defaultIgnoreSearchKeys = [
        'id',
        'createdBy',
        'updatedBy',
        'deletedBy',
    ];
    static getPaginationQuery(data, fields = false) {
        if (fields) {
            fields = fields.filter(
                f => !(RequestParam.defaultIgnoreSearchKeys.indexOf(f) >= 0)
            );
        }

        let { page, sortBy, descending, rowsPerPage, search } = data;

        page = page || 1;
        rowsPerPage = rowsPerPage || 5;
        sortBy = sortBy || false;
        descending = descending || false;
        search = search || false;

        if (rowsPerPage < 0) {
            rowsPerPage = 100000000000;
        }

        let query = { where: {} };

        if (sortBy && fields.indexOf(sortBy) >= 0) {
            query.order = [[sortBy, descending ? 'desc' : 'asc']];
        }

        query.paginate = { page: page, size: rowsPerPage };

        if (fields) {
            if (typeof search == 'string') {
                query.where.$or = [];
                for (let f of fields) {
                    query.where.$or.push({ [f]: { $like: `%${search}%` } });
                }
            } 
            else if (Array.isArray(search)) {
                for (let s of search) {
                    if (fields.indexOf(Object.keys(s)[0]) >= 0) {
                        query.where[Object.keys(s)[0]] = s[Object.keys(s)[0]];
                    }
                }
            }
        }

        return query;
    }
}  