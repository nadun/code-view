//@flow
import to from './await-to';
import db from '../db';
import config from '../config';
import express from './express';
import log from './logging';
import Auth from '../middlewares/Auth';

const PublicRouter = require('../router/PublicRouter');
const AdminRouter = require('../router/AdminRouter');
const ExternalRouter = require('../router/ExternalRouter');
// const auth = require('../auth');

export default class App {
  static async authenticateDatabase() {
    let err, result;
    [err, result] = await to(db.sequelize.authenticate());

    if (err) {
      return Promise.reject(err);
    }
    return Promise.resolve(result);
  }

  static async seedDatabase(db) {
    let promises = [];
    return Promise.all(promises);
  }

  static async syncDatabase() {
    let err, result;
    if (
      ['local', 'staging', 'development', 'test', 'nadun'].indexOf(
        config.env
      ) >= 0
    ) {
      try {
        await App.dropForeignKeyConstraints(db.sequelize);
      } catch (e) {
        console.log('Key drop error - ', e);
      }
      [err, result] = await to(
        db.sequelize.sync({ alter: true, force: false })
      );
      App.seedDatabase(db);
    } else if (['staging'].indexOf(config.env) >= 0) {
      [err, result] = await to(
        db.sequelize.sync({ alter: false, force: false })
      );
    } else {
      [err, result] = await to(
        db.sequelize.sync({ alter: false, force: false })
      );
    }

    if (err) {
      return Promise.reject(err);
    }
    return Promise.resolve(db);
  }

  static async startAppServer(app) {
    try {
      app.listen(config.port, () => {
        log.info(`The server is running at http://localhost:${config.port}`);
      });
      return Promise.resolve(app);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  static async start() {
    try {
      let app = App.addRouting(express);
      await App.authenticateDatabase();
      await App.syncDatabase();
      app = await App.startAppServer(app);
      return Promise.resolve(app);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  static addRouting(app) {
    
    // app.use('/api/v1/auth', auth);
    
    app.use('/admin/api/v1/', AdminRouter);
    app.use('/api/v1/', PublicRouter);
    app.use('/external/api/v1/', ExternalRouter);

    app.get('*', function(req, res) {
      res.render('public/404');
    });

    app.use(function(err, req, res, next) {
      res.json(err);
    });

    return app;
  }

  static async shutdown() {
    return db.sequelize.close();
  }

  static dropForeignKeyConstraints(database) {
    //this is a hack for dev only!
    const queryInterface = database.getQueryInterface();
    return queryInterface
      .showAllTables()
      .then(tableNames => {
        return Promise.all(
          tableNames.map(tableName => {
            return queryInterface
              .showConstraint(tableName)
              .then(constraints => {
                return Promise.all(
                  constraints.map(constraint => {
                    if (constraint.constraintType === 'FOREIGN KEY') {
                      return queryInterface.removeConstraint(
                        tableName,
                        constraint.constraintName
                      );
                    }
                  })
                );
              });
          })
        );
      })
      .then(() => database);
  }
}
