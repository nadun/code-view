//@flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import Base from './base.model';

export class OrderStatus extends Base {
  static init(sequelize) {
    this.tableName = 'orderstatus';
    this.fields = {
        AvailableQuantity: DataTypes.DOUBLE,
        Match: DataTypes.STRING,
        Status: {
            type: DataTypes.STRING,
            defaultValue: 'active'
        }
    };

    return super.init(this.fields, { sequelize, tableName: this.tableName });
  }

  static associate(models) {
    this.belongsTo(models.Order, {foreignKey: 'OrderId'});
  }
}
