import { Model, DataTypes } from 'sequelize';
import _ from 'lodash';

export default class Base extends Model {
  static init(fields, options) {
    this.fields = {
    };

    return super.init(
      _.extend(this.fields, fields),
      _.extend(
        {
          paranoid: true,
          timestamps: true,
        },
        options
      )
    );
  }
}
