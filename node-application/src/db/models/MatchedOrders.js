//@flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import Base from './base.model';

export class MatchedOrders extends Base {
  static init(sequelize) {
    this.tableName = 'matchedorders';
    this.fields = {
        BidAsk: DataTypes.STRING,
        BuyCurrency: DataTypes.STRING,        
        SellCurrency: DataTypes.STRING, 
        Price: DataTypes.DOUBLE,
        CalculatePrice: DataTypes.DOUBLE,
        Quantity: DataTypes.DOUBLE,
        IsConverted: DataTypes.BOOLEAN,
        TradeId: DataTypes.STRING,
        Fee: DataTypes.STRING,
        CalculateFee: DataTypes.STRING,
        MakerOrTaker: {
            type: DataTypes.STRING(10),
            defaultValue: 0
        },
        Status: {
            type: DataTypes.STRING,
            defaultValue: 'active'
        }
    };

    return super.init(this.fields, { sequelize, tableName: this.tableName });
  }

  static associate(models) {
    this.belongsTo(models.Order);
    this.belongsTo(models.Order, {as: "RefOrder"});
  }
}
