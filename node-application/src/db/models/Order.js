//@flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import Base from './base.model';

export class Order extends Base {
  static init(sequelize) {
    this.tableName = 'orders';
    this.fields = {
      UserId: DataTypes.STRING,
      Type: DataTypes.STRING,
      CurrentPosition: DataTypes.STRING,
      ClosePosition: DataTypes.STRING,
      BidAsk: DataTypes.STRING,
      Quantity: DataTypes.DOUBLE,
      AvailableQuantity: DataTypes.DOUBLE,
      Price: DataTypes.DOUBLE,
      BuyCurrency: DataTypes.STRING,
      SellCurrency: DataTypes.STRING,
      Leverage: DataTypes.INTEGER,
      AccountDeduction: DataTypes.DOUBLE,
      PostOnly: {
          type: DataTypes.BOOLEAN,
          defaultValue: 0
      }
    };

    return super.init(this.fields, { sequelize, tableName: this.tableName });
  }

  static associate(models) {
    this.hasMany(models.OrderStatus);
    this.hasMany(models.MatchedOrders);
  }
}
