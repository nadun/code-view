//@flow
import Sequelize, { Model, DataTypes } from 'sequelize';
import Base from './base.model';

export class FeeSettings extends Base {
  static init(sequelize) {
    this.fields = {
      FeeMaker: DataTypes.DOUBLE,
      FeeTaker: DataTypes.DOUBLE,
    };

    return super.init(this.fields, { sequelize });
  }

  static associate(models) {
  }
}