import Sequelize from 'sequelize';
import config from '../config';
import log from '../util/logging';

const injectModels = [
  'Order',
  'MatchedOrders',
  'OrderStatus',
  'FeeSettings'
];

if(process.env.NODE_ENV == "local"){

  var sequelize = new Sequelize(
    config.mysql.database,
    config.mysql.username,
    config.mysql.password,
    {
      host: config.mysql.host,
      port: config.mysql.port,
      dialect: 'mysql',
      operatorsAliases: false, // Disable aliases,
      logging: config.mysql.logging
    }
  );

}else{

  var sequelize = new Sequelize(
    config.mysql.database,
    config.mysql.username,
    config.mysql.password,
    {
      port: config.mysql.port,
      dialect: 'mysql',
      operatorsAliases: false, // Disable aliases,
      logging: config.mysql.logging,
      replication : {
        write: {
          host     : config.mysql.host, 
          username : config.mysql.username, 
          password : config.mysql.password
        },
        read: [
          {
            host : config.mysql.readerHost, 
            username : config.mysql.username, 
            password : config.mysql.password
          }
        ]
      }
    }
  );

}

let models = {};
for (let m of injectModels) {
  models[m] = require(`./models/${m}`)[m].init(sequelize, Sequelize);
}
// Run `.associate` if it exists,
// ie create relationships in the ORM
Object.values(models)
  .filter(model => typeof model.associate === 'function')
  .forEach(model => model.associate(models));

models = {
  ...models,
  sequelize,
};
module.exports = models;
