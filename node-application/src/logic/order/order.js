import _ from 'lodash';

import {
  OrderRepository
} from '../../repositories';

export async function createNewOrder(data) {
  try {

    //1.create order
    let order = await new OrderRepository().create(data);

    return order;
  } catch (e) {
      console.error(e)
    return Promise.reject(e);
  }
}
