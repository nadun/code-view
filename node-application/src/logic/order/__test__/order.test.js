import db from '../../../db';

const { sequelize, Order } = db
const { createNewOrder } = require("../order")

describe('test order functions', () => {
    var userId = "5aae1e96-1da7-4fd7-8bed-511b1b9bd520";

    beforeAll(async done => {
        await Order.destroy({
            where: {},
            force: true,
          });

          done();
    })

    test('create new order', async done => {
        let createdOrder = await createNewOrder({
            userId:  userId,
            quantity: 1,
            price: 959703,
            type: "limit",
            buyCurrency: "BTC",
            sellCurrency: "IDR",
            fixedSide: "buy"
        });

        createdOrder = JSON.parse(JSON.stringify(createdOrder));

        expect(createdOrder).toHaveProperty('id');
        expect(createdOrder).toHaveProperty('createdAt');
        expect(createdOrder).toHaveProperty('updatedAt');

        done();
    })

    afterAll(async done => {
        // Closing the DB connection allows Jest to exit successfully.
        sequelize.close();
        done();
    });

})