module.exports = {
    id: '/Order',
    type: 'object',
    properties: {
        type: { type: 'string' },
        fixedSide: { type: 'string' },
        quantity: { 
            type: 'number' 
        },
        buyCurrency: { type: 'string' },
        sellCurrency: { type: 'string' },
        leverage: { type: 'number' },
    },
    required: ['type', 'fixedSide', 'quantity', 'buyCurrency','sellCurrency']
};
