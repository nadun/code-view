//@flow
import to from './util/await-to';
import app from './util/App';
import config from './config';
function init() {
  app
    .start()
    .then(data => {
      console.log('App started!');
    })
    .catch(e => {
      console.log('App failed - ', e);
    });
}
init();
