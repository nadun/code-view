import { Auth, Validator, Action, Response } from '../middlewares';
import OrderController from '../api/v1/OrderController';
import express from 'express';

const router = express.Router();

router.get(
  '/order/get',
  OrderController.index
);


module.exports = router;
