import { Auth, Validator, Action, Response } from '../middlewares';
import OrderController from '../api/v1/OrderController';
import express from 'express';

const router = express.Router();

router.post(
    '/order/create',
    Validator.validate(require('../validation/newOrder')),
    OrderController.createOrder
);

router.get(
    '/order/getOrders',
    OrderController.index
);

module.exports = router;