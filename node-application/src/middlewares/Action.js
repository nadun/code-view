import config from '../config';

export default class Action {
  static setCreatedBy(req) {
    req.body.createdBy = req.user.id;
    return req;
  }
  static setUpdatedBy(req) {
    req.body.updatedBy = req.user.id;
    return req;
  }
  static setDeleteddBy(req) {
    req.body.deletedBy = req.user.id;
    return req;
  }

  static setAccount(req) {
    req.body.AccountId = req.account.id;
    return req;
  }

  static create(req, res, next) {
    if (req.user) {
      Action.setCreatedBy(req);
    }
    if (req.account) {
      Action.setAccount(req);
    }

    next();
  }
  static update(req, res, next) {
    if (req.user) {
      Action.setUpdatedBy(req);
    }

    if (req.account) {
      Action.setAccount(req);
    }
    next();
  }
  static delete(req, res, next) {
    if (!req.body) {
      req.body = {};
    }
    if (req.user) {
      Action.setDeletedBy(req);
    }

    if (req.account) {
      Action.setAccount(req);
    }
    next();
  }
}
