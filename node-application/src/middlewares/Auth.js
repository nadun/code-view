export default class Auth {
  static isAuthenticated() {
    return async (req, res, next) => {
      if (
        req.method === 'OPTIONS' &&
        req.headers.hasOwnProperty('access-control-request-headers')
      ) {
        var hasAuthInAccessControl = !!~req.headers[
          'access-control-request-headers'
        ]
          .split(',')
          .map(function(header) {
            return header.trim();
          })
          .indexOf('authorization');

        if (hasAuthInAccessControl) {
          next();
        }
      }

      try {
        let result = {auth: "success"}
      } catch (e) {
        console.log(e);
        return res.status(401).json({ code: 'INVALID_USER' });
      }

      next();
    };
  }

  static label(label) {
    return (req, res, next) => {
      req.label = label;
      next();
    };
  }
}
