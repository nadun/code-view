var Ajv = require('ajv');
const log = require('../util/logging');

export default class Validator {
  static validate(schema, data = false) {
    if (data) {
      return Validator.validateData(schema, data);
    }

    return async (req, res, next) => {
      try {
        Validator.validateData(schema, req.body);
        next();
      } catch (e) {
        log.debug(e);
        return res.status(400).json({
          code: 'PARAM_VALIDAION_FAILED',
          errors: e,
        });
      }
    };
  }

  static validateData(schema, data) {
    var ajv = new Ajv({ schemaId: 'auto', allErrors: true }); // options can be passed, e.g. {allErrors: true}
    var validateSchema = ajv.compile(schema);
    var valid = validateSchema(data);

    if (!valid) {
      validateSchema.errors = validateSchema.errors.map(e => {
        return {
          message: e.message,
          property: e.params[Object.keys(e.params)[0]],
        };
      });

      log.debug(validateSchema.errors);

      throw validateSchema.errors;
    }
    return true;
  }
}
