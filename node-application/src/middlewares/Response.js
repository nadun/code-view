export default class Response {
  static json(data) {
    return async (req, res, next) => {
      if (typeof data == 'function') {
        data = await data();
      }

      if (data instanceof Promise) {
        data = await data;
      }

      let status = 200;
      if (data === null) {
        status = 404;
      }
      res.status(status).json(data);
    };
  }

  static error(status = 400, data = {}) {
    return (req, res, next) => {
      res.status(status).json(data);
    };
  }
}
