import _ from 'lodash';
const env = process.env.NODE_ENV || 'local';
const extendedConfig = require(`./${env.toLocaleLowerCase()}`);

let config = {
  appName: 'tokocrypto',
  logging: {
    enabled: true,
    level: 'debug',
  },
  mysql: {
    host: process.env.dbaddr,
    port: process.env.dbport,
    database: process.env.dbname,
    username: process.env.dbUsername,
    password: process.env.dbPassword,
    logging: true
  },
  port: process.env.PORT || 9000,
  env: env,
  jwt: {
    secret: '$eCrEt',
    validity: 60 * 60 * 12,
  },
};

config = _.extend(config, extendedConfig);

module.exports = config
