module.exports = {
  mysql: {
    host: 'localhost',
    port: 3306,
    database: 'tokocrypto',
    username: 'root',
    password: 'mysql',
    logging: true
  },
};
