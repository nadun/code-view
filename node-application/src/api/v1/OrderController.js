import _ from 'lodash';
import { createNewOrder } from '../../logic/order/order';
import RequestParam from '../../util/RequestParam';
import {
    OrderRepository,
    OrderStatusRepository
  } from '../../repositories';

export default class OrderController{

    static orderRepository = new OrderRepository();

    static async index(request, response) {
        try {

          let fields = OrderController.orderRepository.getFields();
          let query = RequestParam.getPaginationQuery(request.body, fields);
          
          return response.json(await new OrderRepository().list(
            _.merge(query, {
                include: [
                    {
                        model: new OrderStatusRepository().model,
                        where: {
                            Status: 'active'
                        }
                    }
                ]
            })
          ));
        } catch (e) {
          return response
            .status(400)
            .json({ code: request.label + '_FAILED', error: e.message });
        }
    }

    static async createOrder(request, response){
        try {
            let data = _.pick(request.body,['userId','type', 'fixedSide', 'quantity', 'price', 'buyCurrency', 'sellCurrency', 'leverage', 'post_only']);
      
            return response.json(
              await createNewOrder({
                    UserId: data.userId,
                    Type: data.type,
                    BidAsk: data.fixedSide,
                    Quantity: data.quantity,
                    AvailableQuantity: data.quantity,
                    Price: data.price,
                    BuyCurrency: data.buyCurrency.toUpperCase(),
                    SellCurrency: data.sellCurrency.toUpperCase(),
                    Leverage: data.leverage || 0,
                    PostOnly: (data.post_only == "true") ? true : false,
                    AccountDeduction: data.quantity
                })
            );
          } catch (e) {
            return response
              .status(400)
              .json({ code: request.label + '_FAILED', error: e.message });
          }
    } 

}