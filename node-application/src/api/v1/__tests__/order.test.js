import db from '../../../db';
const request = require('supertest');
const app = require('../../../util/express'); 

const { sequelize, Order } = db;
import OrderController from '../OrderController';

app.use('/external/api/v1/order/create', OrderController.createOrder)
app.use('/external/api/v1/order/getOrders', OrderController.index)

describe('POST /external/api/v1/order', () => {
    var userId = "5aae1e96-1da7-4fd7-8bed-511b1b9bd520";

    beforeAll(async done => {
        await Order.destroy({
            where: {},
            force: true,
          });

          done();
    })

    test('create new order', async done => {

        let response = await request(app)
            .post('/external/api/v1/order/create')
            .send({
                userId: userId,
                quantity: 1,
                price: 959703,
                type: "limit",
                buyCurrency: "BTC",
                sellCurrency: "IDR",
                fixedSide: "buy"
            })
            .then(result => {
                expect(result).toHaveProperty('status', 200);
                expect(result.body).toHaveProperty('id');
                expect(result.body).toHaveProperty('PostOnly');
                expect(result.body).toHaveProperty('createdAt');
                expect(result.body).toHaveProperty('updatedAt');
                done();
            })
        
    })

    test('get active all orders', async done => {

        let response = await request(app)
            .get('/external/api/v1/order/getOrders')
            .set('Accept', 'application/json');

            expect(response).toHaveProperty('status', 200);
            expect(response.body).toHaveProperty('data');
            expect(Array.isArray(response.body.data)).toBe(true);
            done();
        
    })

    afterAll(async done => {
        sequelize.close();
        done();
    })

})