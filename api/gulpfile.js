var gulp = require('gulp');
var mocha = require('gulp-mocha');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var apidoc = require('gulp-apidoc');

gulp.task('bind-test', function () {
    return gulp.src(['test/test-*.js', 'src/**/*.test.js'])
        .pipe(concat('tests.js'))
        .pipe(gulp.dest('.tmp'));
});

gulp.task('test', function () {
    return gulp.src(['./src/**/*.test.js','./test/test-api.js'], { read: false })
        .pipe(mocha({
            reporter: 'spec'
        }));
});

gulp.task('apidoc', function (done) {
    apidoc({
        src: "src/",
        dest: "apidoc/",
        includeFilters: [".*\\.controller\\.js$"]
    }, done);
});