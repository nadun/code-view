var sinon = require('sinon');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should;
var expect = chai.expect;
var supertest = require('supertest');
chai.use(chaiHttp);

var config = require('../src/config');
var api = supertest('http://'+config.ip+':'+config.port);
var db = require('../src/lib/db');
