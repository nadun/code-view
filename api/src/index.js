import http from 'http';
import express from 'express';
import CognitoExpress from 'cognito-express'
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import config from './config';
import pluginLoader from './lib/plugin.loader';
import responseHandler from './lib/response.handler';
import roles from './auth/roles';


//create app
let app = express(),authenticatedRoute = express.Router();

//get env details
app.set('config', config);


//create api server
app.server = http.createServer(app);

// logger
app.use(morgan('dev'));

// 3rd party middleware
app.use(cors({
    exposedHeaders: config.exposedHeaders
}));

//body parser for api calls
app.use(bodyParser.json({
    limit: config.bodyParserLimit
}));
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

//inject response handler
app.use(function(req, res, next){
    req.handleResponse = responseHandler;
    next();
});

app.use("/", authenticatedRoute);

//inject config to request
app.use(function(req, res, next){
    req.config = config;
    next();
});



//body parser for api calls
pluginLoader(app);


//Initializing CognitoExpress constructor
// const cognitoExpress = new CognitoExpress({
//     region: config.region,
//     cognitoUserPoolId: config.cognitoPool,
//     tokenUse: "access", //Possible Values: access | id
//     tokenExpiration: 3600000 //Up to default expiration of 1 hour (3600000 ms)
// });

// //Our middleware that authenticates all APIs under our 'authenticatedRoute' Router
// authenticatedRoute.use(function(req, res, next) {
    
//     //I'm passing in the access token in header under key accessToken
//     console.log(req.headers)
//     let accessTokenFromClient = req.headers.authorization;
 
//     //Fail if token not present in header. 
//     if (!accessTokenFromClient) return res.status(401).send("Access Token missing from header");
 
//     cognitoExpress.validate(accessTokenFromClient, function(err, response) {
        
//         //If API is not authenticated, Return 401 with error message. 
//         if (err) return res.status(401).send(err);
        
//         //Else API has been authenticated. Proceed.
//         res.locals.user = response;
//         next();
//     });
// });

// //Default Page
// authenticatedRoute.route('/').get((req, res) => {
//     res.send('Welcome to PETPOS API');
// });

// authenticatedRoute.get("/myfirstapi", roles.hasRole(['employee']), function(req, res, next) {

//         res.send(`Hi ${res.locals.user.username}, your API call is authenticated!`);
        
// });




//start api server
app.server.listen(config.port, config.ip, function () {    
    console.log(`Started on port ${app.server.address().port}`);
});
 

