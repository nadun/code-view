module.exports = function (sequelize, DataTypes) {
    var Pet = sequelize.define('Pet', {
        type: {
            type: DataTypes.STRING,
            unique: 'type_deletedAt_const'
        },
        default: DataTypes.BOOLEAN,   
        deletedAt: {
            type: DataTypes.DATE,
            unique: 'type_deletedAt_const'
        }
    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    Pet.addHook('afterDestroy', 'deleteBreeds', (pet, options) => {
        return sequelize.models.Breed.destroy({ where: { PetId: pet.id } })
    });

    return Pet;
}