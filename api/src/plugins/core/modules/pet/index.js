let express = require('express');

let controller = require('./Pet.controller');

let router = express.Router();

module.exports = function (app, db) {

    db.Pet = db.sequelize.import('./Pet.model');

    router.get('/', controller.index);
    router.get('/:id([0-9]+)', controller.show);
    router.post('/', controller.create);
    router.put('/:id([0-9]+)', controller.update);
    router.delete('/:id([0-9]+)', controller.destroy);
    router.post('/delete/', controller.deleteAll);
    router.put('/default/:id([0-9]+)', controller.updateDefault);
    
    return {
        router: router,
        relateModels: function () {
            db.Pet.hasMany(db.Breed);
        }
    }

};
