module.exports = function (sequelize, DataTypes) {
    var Registration_type = sequelize.define('Registration_type', {
        type: DataTypes.STRING,
        default: DataTypes.BOOLEAN,
    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    return Registration_type;
}