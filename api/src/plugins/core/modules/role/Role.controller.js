
export function index(req, res, next) {
    req.db.Role.findAll({
        attributes: ["id","role_name","status","actions"],
        
    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}


export function show(req, res, next) {
    req.db.Role.find({
        where: {
            id: req.params.id
        },
        attributes: ["id","role_name","status","actions"],
        
    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}



export function create(req, res, next) {

    if (req.body.id) delete req.body.id;

    req.db.Role.create(req.body)
        .then(req.handleResponse.respond(res, 201))
        .catch(req.handleResponse.handleError(res));

}


export function update(req, res, next) {

    req.db.Role.find({
        where: {
            id: req.params.id
        },
        attributes: ["id","role_name","status","actions"],
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {

                obj.update(req.body, {
                    fields: ["id","role_name","status","actions"],
                })
                    .then(req.handleResponse.respond(res))
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));

}



export function destroy(req, res, next) {
    req.db.Role.find({
        where: {
            id: req.params.id
        }
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {

                return obj
                    .destroy()
                    .then(() => {
                        return res.status(204).end();
                    })
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));
}
