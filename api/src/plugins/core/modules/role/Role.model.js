module.exports = function (sequelize, DataTypes) {
    var Role = sequelize.define('Role', {
       role_name: DataTypes.STRING,
        status: DataTypes.STRING,
        actions: DataTypes.STRING,
                
    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    return Role;
}