let express = require('express');

let controller = require('./breeder.controller');
        
let router = express.Router();

module.exports = function (app, db) {
    
    db.Breeder = db.sequelize.import('./breeder.model');
        
    router.get('/', controller.index); 
    router.get('/:id([0-9]+)', controller.show); 
    router.post('/', controller.create); 
    router.put('/:id([0-9]+)', controller.update); 
    router.delete('/:id([0-9]+)', controller.destroy); 
    
    
    return { 
        router: router,
        relateModels: function(){
            // db.Breeder.hasOne(db.MedicalRecord);
        }
    }

};
