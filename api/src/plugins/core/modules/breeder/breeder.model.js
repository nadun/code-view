module.exports = function (sequelize, DataTypes) {
    var Breeder = sequelize.define('Breeder', {
        breeder: DataTypes.STRING,
        usda_no: DataTypes.STRING,
        address: DataTypes.STRING,
        phone: DataTypes.STRING
    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    return Breeder;
}