let express = require('express');

let controller = require('./UserRole.controller');
        
let router = express.Router();

module.exports = function (app, db) {

    db.UserRole = db.sequelize.import('./UserRole.model');

    router.get('/', controller.index); 
    router.get('/:id([0-9]+)', controller.show); 
    router.post('/', controller.create); 
    router.put('/:id([0-9]+)', controller.update); 
    router.delete('/:id([0-9]+)', controller.destroy); 
    
    return { 
        router: router,
        relateModels: function(){
            db.UserRole.belongsTo(db.User);           
            db.UserRole.belongsTo(db.Role);
        }
    }

};
