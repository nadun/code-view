module.exports = function (sequelize, DataTypes) {
    var Administered_type = sequelize.define('Administered_type', {
        type: DataTypes.STRING,
    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    return Administered_type;
}