
export function index(req, res, next) {
    req.db.Administered_type.findAll({
        attributes: ["id", "type"],

    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}


export function show(req, res, next) {
    req.db.Administered_type.find({
        where: {
            id: req.params.id
        },
        attributes: ["id", "type"],

    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}



export function create(req, res, next) {

    if (req.body.id) delete req.body.id;

    req.db.Administered_type.create(req.body)
        .then(req.handleResponse.respond(res, 201))
        .catch(req.handleResponse.handleError(res));

}


export function update(req, res, next) {

    req.db.Administered_type.find({
        where: {
            id: req.params.id
        },
        attributes: ["id", "type"],
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {

                obj.update(req.body, {
                    fields: ["id", "type"],
                })
                    .then(req.handleResponse.respond(res))
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));

}



export function destroy(req, res, next) {
    req.db.Administered_type.find({
        where: {
            id: req.params.id
        }
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {

                return obj
                    .destroy()
                    .then(() => {
                        return res.status(204).end();
                    })
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));
}

export function updateDefault(req, res, next) {
    if (req.body.default != true) {

        req.body.default = !req.body.default; //toggle Default if true
    }

    req.db.Administered_type.find({
        where: {
            id: req.params.id
        },
        attributes: ["id", "type"],
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            return Promise.all([
                obj.updateAttributes(req.body),
                req.db.Administered_type.findAll({ where: { default: req.body.default }, attributes: ["id", "type"], }).then(req.handleResponse.checkNotFound(res))
                    .then((rts) => {
                        let rtArray = [];
                        for (let rt of rts) {
                            rtArray.push(rt.dataValues);
                        }
                        req.db.Administered_type.bulkCreate(rtArray, { updateOnDuplicate: true })
                    })

            ])

        }).then(req.handleResponse.respond(res))
        .catch(err => {
            console.log(err);
        })

}

export function deleteAll(req, res, next) {
    req.db.Administered_type.destroy({
        where: {
            id: req.body.ids
        },
        individualHooks: true
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {
            return res.status(200).json(obj);
        })
        .catch(req.handleResponse.handleError(res));
}
