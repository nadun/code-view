let express = require('express');

let controller = require('./administered_type.controller');

let router = express.Router();

module.exports = function (app, db) {

    db.Administered_type = db.sequelize.import('./administered_type.model');

    router.get('/', controller.index);
    router.get('/:id([0-9]+)', controller.show);
    router.post('/', controller.create);
    router.put('/:id([0-9]+)', controller.update);
    router.delete('/:id([0-9]+)', controller.destroy);
    router.post('/delete/', controller.deleteAll);
    router.put('/default/:id([0-9]+)', controller.updateDefault);

    return {
        router: router,
        relateModels: function () {
            // db.Registration_type.hasMany(db.MedicalRecord);
        }
    }

};
