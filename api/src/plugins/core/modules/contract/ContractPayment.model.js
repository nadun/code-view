module.exports = function (sequelize, DataTypes) {
    var ContractPayment = sequelize.define('ContractPayment', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        method: DataTypes.STRING,
        amount: DataTypes.DECIMAL(10,2),
        card_type: DataTypes.STRING,
        last_digits: DataTypes.INTEGER,
    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    return ContractPayment;
}