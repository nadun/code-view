import { isArray } from "util";
import async from "async";
import dateFormat from 'dateformat';

export function index(req, res, next) {
    req.db.Contract.findAll({
        attributes: ["id", "price", "tax", "total", "date"],
        include: [
            {
                model: req.db.MedicalRecord,
                attributes: ["id", "pet_id"],
                include: [
                    {model: req.db.Pet, attributes: ["id", "type", "default"]},
                    {model: req.db.Breed, attributes: ["id", "breed_type"]}
                ]
            },
            {model: req.db.Customer, attributes: ["id", "name", "phone_cell"]}
        ],
        where: {
            '$MedicalRecord.id$' : {$ne: null}
        }
        
    }).then(records => {

        let recs = JSON.parse(JSON.stringify(records));
        recs.forEach(rec => {
            if(rec.MedicalRecord) {
                rec.pet_id = rec.MedicalRecord.pet_id;
                rec.pet_type = rec.MedicalRecord.Pet.type;
                rec.breed_type = rec.MedicalRecord.Breed.breed_type;
            }
            if(rec.Customer) {
                rec.cus_name = rec.Customer.name;
                rec.cus_cell = rec.Customer.phone_cell;
            }
            
        });

        return recs;
            
    })
        .then(req.handleResponse.respond(res))
        .catch(e=>{
            console.log(e);
            req.handleResponse.handleError(res)
        });
}

export function show(req, res, next) {
    req.db.Contract.find({
        where: {
            id: req.params.id
        },
        attributes: ["id", "price", "tax", "total", "date"],
        include: [
            {
                model: req.db.MedicalRecord,
                attributes: ["id", "pet_id", "date_of_birth", "sex", "color_markings", "micro_chip", "avatar_path", "avatar_title", "avatar_file_name"],
                include: [
                    {model: req.db.Pet, attributes: ["id", "type"]},
                    {model: req.db.Breed, attributes: ["id", "breed_type"]}
                ]
            },
            {
                model: req.db.Customer,
                attributes: ["id", "name", "phone_cell", "phone_home", "address", "city", "state", "email", "zip_code", "find_us"]
            },
            {
                model: req.db.ContractPayment,
                attributes: ["id", "method", "amount", "card_type", "last_digits"]
            }
        ]
        
    }).then(record => {

        let rec = JSON.parse(JSON.stringify(record));
                    
        rec.pet_id = rec.MedicalRecord.pet_id;
        rec.MedicalRecord.date_of_birth = dateFormat(rec.MedicalRecord.date_of_birth, "yyyy-mm-dd");
        rec.sex = rec.MedicalRecord.sex;
        rec.color_markings = rec.MedicalRecord.color_markings;
        rec.micro_chip = rec.MedicalRecord.micro_chip;
        rec.pet_type = rec.MedicalRecord.Pet.type;
        rec.breed_type = rec.MedicalRecord.Breed.breed_type;
        rec.cus_name = rec.Customer.name;
        rec.cus_cell = rec.Customer.phone_cell;
        rec.cus_home = rec.Customer.phone_home;
        rec.cus_address = rec.Customer.address;
        rec.cus_city = rec.Customer.city;
        rec.cus_state = rec.Customer.state;
        rec.cus_email = rec.Customer.email;
        rec.cus_zip = rec.Customer.zip_code;
        rec.cus_findus = rec.Customer.find_us;
        rec.date = dateFormat(rec.date, "yyyy-mm-dd");
        

        return rec;
            
    })
        .then(req.handleResponse.respond(res))
        .catch(e=>{
            console.log(e);
            req.handleResponse.handleError(res)
        });
}

export function create(req, res, next) {

    if (req.body.id) delete req.body.id;
        console.log("*************called 3 *********************", req.body);
        return new Promise((resolve, reject) => {
            req.db.Customer.find({
                where: {
                    email: req.body.email.trim(),
                    $or: [
                        {
                            phone_cell: {$eq: req.body.worknumber.trim()}
                        }
                    ]
                }
            }).then(customer => {
                if(!customer) {
                    let cust = {
                        name: req.body.cusname,
                        address: req.body.saddress,
                        city:req.body.city,
                        state:req.body.state,  
                        zip_code:req.body.zip,
                        phone_home:req.body.homenumber.trim(),
                        phone_cell:req.body.worknumber.trim(),
                        email:req.body.email.trim(),
                        find_us:req.body.hfindus,
                    }
                    req.db.Customer.create(cust)
                        .then(c => {
                            return resolve(c);
                        })
                } else {
                    return resolve(customer)
                }
                
            }).catch(req.handleResponse.handleError(res));

        })

        .then(customer => {

            console.log("*************called 4 *********************", req.body, customer);

            req.body.price = parseFloat(req.body.price);
            req.body.tax = parseFloat(req.body.tax);
            req.body.total = parseFloat(req.body.price) + parseFloat(req.body.tax);
            req.body.MedicalRecordId = parseInt(req.body.petId) ;
            req.body.CustomerId = customer.id;

            req.db.Contract.create(req.body)
                .then(res => {

                    let resp = res.get({
                        plain: true
                    })
                    let objArr = [];

                    for (let va of req.body.payments) {
                        va.ContractId = resp.id;
                        va.amount = parseFloat(va.amount);
                        objArr.push(va);
                    }

                    console.log("-----------aaa--------------", objArr);

                    return req.db.ContractPayment.bulkCreate(objArr)
                        .then(res2 => {
                            console.log("-----------sef--------------", res2);

                            resp.payments = objArr;

                            return resp;
                        })
                })
                .then(req.handleResponse.respond(res, 201))
                .catch(req.handleResponse.handleError(res));

        })

}


// export function update(req, res, next) {

//     req.db.MedicalRecord.find({
//         where: {
//             id: req.params.id
//         }
//     })
//         .then(req.handleResponse.checkNotFound(res))
//         .then((obj) => {

//             if (obj) {

//                 obj.update(req.body, {
//                     fields: ["id", "description", "actions", "PetId", "PetStoreId", "PetId", "PetStoreId"],
//                 })
//                     .then(req.handleResponse.respond(res))
//                     .catch(req.handleResponse.handleError(res));
//             }
//         })
//         .catch(req.handleResponse.handleError(res));

// }



export function destroy(req, res, next) {
    req.db.Contract.find({
        where: {
            id: req.params.id
        }
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {

                return obj
                    .destroy()
                    .then(() => {
                        return res.status(204).end();
                    })
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));
}

export function deleteAll(req, res, next) {
    req.db.Contract.destroy({
        where: {
            id: req.body.ids
        },
        individualHooks: true
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {
            return res.status(200).json(obj);
        })
        .catch(req.handleResponse.handleError(res));
}
