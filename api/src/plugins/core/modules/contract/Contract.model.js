module.exports = function (sequelize, DataTypes) {
    var Contract = sequelize.define('Contract', {
        date: DataTypes.DATE,
        price: DataTypes.DECIMAL(10, 2),
        tax: DataTypes.DECIMAL(10, 2),
        total: DataTypes.DECIMAL(10, 2),     
    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    return Contract;
}