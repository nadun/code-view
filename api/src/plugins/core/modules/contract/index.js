let express = require('express');

let controller = require('./Contract.controller');
        
let router = express.Router();

module.exports = function (app, db) {

    db.Contract = db.sequelize.import('./Contract.model');
    db.ContractPayment = db.sequelize.import('./ContractPayment.model');
        
    router.get('/', controller.index); 
    router.get('/:id([0-9]+)', controller.show); 
    router.post('/', controller.create); 
    // router.put('/:id([0-9]+)', controller.update); 
    router.delete('/:id([0-9]+)', controller.destroy);
    router.post('/delete/', controller.deleteAll);

    return { 
        router: router,
        relateModels: function(){

            db.Contract.belongsTo(db.MedicalRecord);
            db.MedicalRecord.hasOne(db.Contract);
            db.Contract.belongsTo(db.Customer);

            db.Contract.hasMany(db.ContractPayment);
            db.ContractPayment.belongsTo(db.Contract);
            
        }
    }

};
