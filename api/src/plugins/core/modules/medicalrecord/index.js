let express = require('express');

let controller = require('./MedicalRecord.controller');
        
let router = express.Router();

module.exports = function (app, db) {

    db.MedicalRecord = db.sequelize.import('./MedicalRecord.model');
    db.MedicalRecordVaccination = db.sequelize.import('./MedicalRecordVaccination.model');
        
    router.get('/', controller.index); 
    router.get('/:id([0-9]+)', controller.show); 
    router.post('/', controller.create); 
    router.put('/:id([0-9]+)', controller.update); 
    router.delete('/:id([0-9]+)', controller.destroy); 
    router.post('/delete/', controller.deleteAll);
    
    router.get('/getLeftMedicalRecords', controller.getLeftMedicalRecords);
    router.post('/attachAvatar', controller.attachAvatar);
    router.post('/addVaccination', controller.addVaccination);
    router.post('/deleteVaccination', controller.deleteVaccination);

    return { 
        router: router,
        relateModels: function(){

            db.MedicalRecord.belongsTo(db.Pet);
            db.MedicalRecord.belongsTo(db.Breed);
            db.MedicalRecord.belongsTo(db.Breeder);
            db.MedicalRecord.belongsTo(db.Registration_type);


            db.MedicalRecord.hasMany(db.MedicalRecordVaccination);
            db.MedicalRecordVaccination.belongsTo(db.MedicalRecord);


            db.MedicalRecordVaccination.belongsTo(db.Vaccination);
            db.Vaccination.hasMany(db.MedicalRecordVaccination);

            
        }
    }

};
