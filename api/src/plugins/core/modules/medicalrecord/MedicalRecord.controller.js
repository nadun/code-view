import { isArray } from "util";
import async from "async";
import dateFormat from 'dateformat';
import buffer from 'buffer';
import config from '../../../../config';
import AWS from 'aws-sdk';
import fs from 'fs';

export function index(req, res, next) {
    req.db.MedicalRecord.findAll({
        attributes: ["id", "arrival_date", "sex", "color_markings", "date_of_birth", "micro_chip", "cross_breed", "registration_type", "sire", "sire_number", "dam", "dam_number", "pet_id", "avatar_path", "avatar_title", "avatar_file_name"],
        include: [{model: req.db.Pet, attributes: ["id", "type", "default"]}, {model: req.db.Breed, attributes: ["id", "breed_type"]}, {model: req.db.Breeder, attributes: ["id", "breeder", "usda_no", "address", "phone"]}, {model: req.db.Registration_type, attribute: ["id", "type", "default"]}, {model: req.db.Contract, attribute: ["id", "date", "price"]}]
    }).then(records => {

        let recs = JSON.parse(JSON.stringify(records));
        recs.forEach(rec => {
            
            rec.pet_type = rec.Pet.type;
            rec.breed_type = rec.Breed.breed_type;
            rec.reg_type = rec.Registration_type.type;
            rec.date_of_birth = dateFormat(rec.date_of_birth, "yyyy-mm-dd");
            console.log(rec);
        });

        return recs;
            
    })
        .then(req.handleResponse.respond(res))
        .catch(e=>{
            console.log(e);
            req.handleResponse.handleError(res)
        });
}

export function getLeftMedicalRecords(req, res, next) {
    req.db.MedicalRecord.findAll({
        attributes: ["id", "arrival_date", "sex", "color_markings", "date_of_birth", "micro_chip", "cross_breed", "registration_type", "sire", "sire_number", "dam", "dam_number", "pet_id", "avatar_path", "avatar_title", "avatar_file_name"],
        include: [{model: req.db.Pet, attributes: ["id", "type", "default"]}, {model: req.db.Breed, attributes: ["id", "breed_type"]}, {model: req.db.Breeder, attributes: ["id", "breeder", "usda_no", "address", "phone"]}, {model: req.db.Registration_type, attribute: ["id", "type", "default"]}, {model: req.db.Treatment, required:false}, {model: req.db.Contract, required:false}],
        where: { '$Contract.MedicalRecordId$': {$eq: null} }
    }).then(records => {

        let recs = JSON.parse(JSON.stringify(records));
        recs.forEach(rec => {
            
            rec.pet_type = rec.Pet.type;
            rec.breed_type = rec.Breed.breed_type;
            rec.reg_type = rec.Registration_type.type;
            rec.date_of_birth = dateFormat(rec.date_of_birth, "yyyy-mm-dd");
        });

        return recs;
            
    })
        .then(req.handleResponse.respond(res))
        .catch(e=>{
            console.log(e);
            req.handleResponse.handleError(res)
        });
}


export function show(req, res, next) {
    req.db.MedicalRecord.find({
        where: {
            id: req.params.id
        },
        attributes: ["id", "arrival_date", "sex", "color_markings", "date_of_birth", "micro_chip", "cross_breed", "registration_type", "sire", "sire_number", "dam", "dam_number", "pet_id", "avatar_path", "avatar_title", "avatar_file_name"],
        include: [
            {model: req.db.Pet, attributes: ["id", "type", "default"]},
            {model: req.db.Breed, attributes: ["id", "breed_type"]},
            {model: req.db.Breeder, attributes: ["id", "breeder", "usda_no", "address", "phone"]},
            {model: req.db.Registration_type, attribute: ["id", "type", "default"]},
            {model: req.db.MedicalRecordVaccination, attribute: ["id", "date", "more_info"], include: [{model: req.db.Vaccination, attribute: ["id", "name"]}]},
            {model: req.db.Treatment}
        ],
    }).then(record => {

        let rec = JSON.parse(JSON.stringify(record));
            
            rec.pet_type = rec.Pet.type;
            rec.breed_type = rec.Breed.breed_type;
            rec.reg_type = rec.Registration_type.type;
            rec.date_of_birth = dateFormat(rec.date_of_birth, "yyyy-mm-dd");
            rec.arrival_date = dateFormat(rec.arrival_date, "yyyy-mm-dd");

            if(rec.Breeder) {
                rec.breeder_name = rec.Breeder.breeder;
                rec.breeder_usda_no = rec.Breeder.usda_no;
                rec.breeder_address = rec.Breeder.address;
                rec.breeder_phone = rec.Breeder.phone;
            }

            rec.MedicalRecordVaccinations.map (v => {
                v.date = dateFormat(v.date, "yyyy-mm-dd");
            })
            
        return rec;
            
    })
        .then(req.handleResponse.respond(res))
        .catch(e=>{
            console.log(e);
            req.handleResponse.handleError(res)
        });
}



export function create(req, res, next) {

    if (req.body.id) delete req.body.id;
        return new Promise((resolve, reject) => {
            if(req.body.newBreeder) {
                let breeder = {
                    breeder: req.body.breederName,
                    usda_no: req.body.usdaNo,
                    address: req.body.breederAddress,
                    phone: req.body.breederPhone,
                }
                req.db.Breeder.create(breeder)
                    .then(c => {
                        return resolve(c);
                    }).catch(req.handleResponse.handleError(res));
            } else {
                req.db.Breeder.find({
                    where: {
                        id: parseInt(req.body.breeder)
                    }
                }).then(b => {
                    return resolve(b);
                }).catch(req.handleResponse.handleError(res));
            }
                    
        })
        .then(breeder => {
            req.body.BreederId = breeder.id

            console.log("*************called 3 *********************");
            return new Promise((resolve, reject) => {
                let vaccinationsArr = [];
                if (isArray(req.body.vaccines)) {
                    async.each(req.body.vaccines, function (vaccination, callback) {
                        console.log("**vacc**", vaccination)
                        vaccinationsArr.push({
                            id: vaccination.VaccinationId,
                            date: vaccination.date,
                            more_info: vaccination.more_info
                        });
                        callback();

                    }, function (err) {
                        if (err) {
                            console.log('A file failed to process');
                            reject();
                        } else {

                            return resolve(vaccinationsArr);
                        }
                    });
                } else {
                    return resolve(vaccinationsArr);
                }
            })
            .then(responseVaccinationsArr => {

                console.log("*************called 4 *********************", responseVaccinationsArr, req.body);

                req.body.arrival_date = req.body.arrival;
                req.body.PetId = req.body.top;
                req.body.BreedId = req.body.breed;
                req.body.date_of_birth = req.body.dob;
                req.body.RegistrationTypeId = req.body.regType;
                // req.body.MedicalRecordVaccination = responseVaccinationsArr;

                return new Promise((resolve, reject) => {
                    var avatar = {};
                    if(req.body.avatar) {
                        var buf = decodeBase64Image(req.body.avatar);

                        AWS.config = config.s3Config.credentials;
                        var key = (new Date()).getTime() + '_' + Math.floor((Math.random() * 1000000) + 1) + '.jpg';
                        var s3 = new AWS.S3();
                        var path = 'uploads/medical-records/avatar';
                        s3.putObject({
                            Bucket: config.s3Config.bucket + '/' + path,
                            Key: key,
                            Body: buf.data,
                            ACL: 'public-read',

                            ContentType: "image/jpeg",
                            // ContentEncoding: "base64"
                        }, function(error, data) {
                            if(error) {
                                console.log('Avatar failed to upload');
                                reject();
                            }
                            else
                                return resolve ({path: path, title: req.body.avatar_title, key: key})
                        });
                    } else {
                        return resolve (avatar)
                    }
                }).then(avatar => {

                    req.body.avatar_path = avatar.path;
                    req.body.avatar_title = avatar.title;
                    req.body.avatar_file_name = avatar.key;
                    req.db.MedicalRecord.create(req.body)
                        .then(res => {

                            let resp = res.get({
                                plain: true
                            })
                            let objArr = [];

                            if(responseVaccinationsArr.length>0) {
                                for (let va of responseVaccinationsArr) {
                                    objArr.push({ MedicalRecordId: resp.id, VaccinationId: va.id, date: va.date, more_info: va.more_info })
                                }
        
                                console.log("-----------aaa--------------", objArr);
        
                                return req.db.MedicalRecordVaccination.bulkCreate(objArr)
                                    .then(res2 => {
                                        console.log("-----------sef--------------", res2);
        
                                        // let resp2 = res2.get({
                                        //     plain: true
                                        // })
        
                                        resp.vaccination = objArr;
        
        
        
                                        return resp;
                                    })
                            } else {
                                return resp;
                            }

                            
                        })
                        .then(req.handleResponse.respond(res, 201))
                        .catch(e=>{
                            console.log(e);
                            req.handleResponse.handleError(res)
                        });
                })
                

            })
        })

}

export function addVaccination(req, res, next) {
    // console.log('bodyyyy', req.body);
    // req.db.MedicalRecordVaccination.create(req.body)
    //                 .then(req.handleResponse.respond(res))
    //                 .catch(e=>{
    //                     console.log(e);
    //                     req.handleResponse.handleError(res)
    //                 });
    req.db.MedicalRecord.find({
        where: {
            id: req.body.MedicalRecordId
        }
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {
            if (obj) {

                Promise.resolve(req.db.MedicalRecordVaccination.create(req.body))
                    .then(req.handleResponse.respond(res))
                    .catch(e=>{
                        console.log(e);
                        req.handleResponse.handleError(res)
                    });
            }
        })
        .catch(req.handleResponse.handleError(res));

}

export function deleteVaccination(req, res, next) {
    req.db.MedicalRecordVaccination.find({
        where: {
            id: req.body.id
        }
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {                
                return obj
                    .destroy()
                    .then(req.handleResponse.respond(res))
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));
}


export function update(req, res, next) {

    req.db.MedicalRecord.find({
        where: {
            id: req.params.id
        }
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {
            
            if (obj) {
                return new Promise((resolve, reject) => {
                    if(req.body.BreederId) {
                        req.db.Breeder.find({
                            where: {
                                id: parseInt(req.body.BreederId)
                            }
                        }).then(b => {
                            return resolve();
                        }).catch(req.handleResponse.handleError(res));
                    } else {
                        return resolve()
                    }
                })
                .then(() => {
                    
                    obj.update(req.body, {
                        fields: ["arrival_date", "sex", "color_markings", "date_of_birth", "micro_chip", "cross_breed", "registration_type", "sire", "sire_number", "dam", "dam_number", "pet_id", "avatar_path", "avatar_title", "PetId", "BreedId", "BreederId", "RegistrationTypeId", "PetShopId"],
                    })
                        .then(req.handleResponse.respond(res))
                        .catch(req.handleResponse.handleError(res));
                })
            }
            
        })
        .catch(req.handleResponse.handleError(res));

}



export function destroy(req, res, next) {
    req.db.MedicalRecord.find({
        where: {
            id: req.params.id
        }
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {
                console.log('objjjjj', obj);

                return obj
                    .destroy()
                    .then(req.handleResponse.respond(res))
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));
}

export function deleteAll(req, res, next) {
    req.db.MedicalRecord.destroy({
        where: {
            id: req.body.ids
        },
        individualHooks: true
    })
        .then(req.handleResponse.checkNotFound(res))
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}


function decodeBase64Image(dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
      response = {};
  
    if (matches.length !== 3) {
      return new Error('Invalid input string');
    }
  
    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');
  
    return response;
}


export function attachAvatar(req, res) {
    // console.log('fileeeee', req.body.file);
    var buf = decodeBase64Image(req.body.file);
    //console.log('buffffff', buf.data);


    //fs.writeFile('test.jpg', buf.data);
    AWS.config = config.s3Config.credentials;
    var key = (new Date()).getTime() + '_' + Math.floor((Math.random() * 1000000) + 1) + '.jpg';
    var s3 = new AWS.S3();
    s3.putObject({
        Bucket: config.s3Config.bucket,
        Key: key,
        Body: buf.data,
        ACL: 'public-read',

        ContentType: "image/jpeg",
        // ContentEncoding: "base64"
    }, function(error, data) {
        if(error)
            req.handleResponse.handleError(res)
        else
            return res.status(200).json(data);
    });
}
