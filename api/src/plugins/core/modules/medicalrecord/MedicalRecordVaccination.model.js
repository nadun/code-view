module.exports = function (sequelize, DataTypes) {
    var MedicalRecordVaccination = sequelize.define('MedicalRecordVaccination', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        date: DataTypes.DATE,
        more_info: DataTypes.STRING,    
    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    return MedicalRecordVaccination;
}