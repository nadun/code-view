module.exports = function (sequelize, DataTypes) {
    var MedicalRecord = sequelize.define('MedicalRecord', {
       arrival_date: DataTypes.DATE,
       sex: DataTypes.ENUM('male', 'female'),
       color_markings: DataTypes.STRING,
       date_of_birth: DataTypes.DATE,
       micro_chip: DataTypes.STRING,
       cross_breed: DataTypes.ENUM('yes', 'no'),
       registration_type: DataTypes.STRING,         
       sire: DataTypes.STRING,         
       sire_number: DataTypes.STRING,         
       dam: DataTypes.STRING,         
       dam_number: DataTypes.STRING,
       pet_id: DataTypes.STRING,
       avatar_path: DataTypes.STRING,
       avatar_title: DataTypes.STRING,
       avatar_file_name: DataTypes.STRING,
    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    return MedicalRecord;
}