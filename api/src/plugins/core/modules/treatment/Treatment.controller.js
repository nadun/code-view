
import dateformat from 'dateformat';

export function index(req, res, next) {

    req.db.Treatment.findAll({
       include: [req.db.MedicalRecord, req.db.Calculations]
    })
    .then(records => {

        let recs = JSON.parse(JSON.stringify(records));

        recs.forEach(rec => {
            console.log("Records",rec);
            rec.pet_id = rec.MedicalRecord.pet_id;
            rec.calculation_name = rec.Calculation.treatmentName;
            rec.calculation_type = rec.Calculation.calculationType;
        });

        return recs;
            
    })
    .then(req.handleResponse.respond(res))
    .catch(e=>{
        console.log(e);
        req.handleResponse.handleError(res)
    });
}


export function show(req, res, next) {
    req.db.Treatment.find({
        where: {
            id: req.params.id
        },
        attributes: ["id","description","actions","PetId","PetStoreId","PetId","PetStoreId"],
        include:[req.db.Pet,req.db.PetStore,]
    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}



export function create(req, res, next) {

    if (req.body.id) delete req.body.id;

    req.db.Treatment.create(req.body)
        .then(req.handleResponse.respond(res, 201))
        .catch(req.handleResponse.handleError(res));

}


export function update(req, res, next) {

    req.db.Treatment.find({
        where: {
            id: req.params.id
        },
        attributes: ["id","description","actions","PetId","PetStoreId","PetId","PetStoreId"],
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {

                obj.update(req.body, {
                    fields: ["id","description","actions","PetId","PetStoreId","PetId","PetStoreId"],
                })
                    .then(req.handleResponse.respond(res))
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));

}



export function destroy(req, res, next) {
    req.db.Treatment.find({
        where: {
            id: req.params.id
        }
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {

                return obj
                    .destroy()
                    .then(() => {
                        return res.status(204).end();
                    })
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));
}

export function deleteAll(req, res, next) {
    req.db.Treatment.destroy({
        where: {
            id: req.body.ids
        },
        individualHooks: true
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {
            return res.status(200).json(obj);
        })
        .catch(req.handleResponse.handleError(res));
}
