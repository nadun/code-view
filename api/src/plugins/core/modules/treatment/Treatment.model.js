module.exports = function (sequelize, DataTypes) {
    var Treatment = sequelize.define('Treatment', {
        start_date: {
            type: DataTypes.DATE
        },
        end_date: {
            type: DataTypes.DATE
        },
    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    return Treatment;
}