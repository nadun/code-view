let express = require('express');


let controller = require('./Treatment.controller');
        
let router = express.Router();

module.exports = function (app, db) {

    
db.Treatment = db.sequelize.import('./Treatment.model');
        
    

router.get('/', controller.index); 
router.post('/', controller.create); 
router.post('/delete/', controller.deleteAll);


return { 
    router: router,
    relateModels: function(){
        db.Treatment.belongsTo(db.MedicalRecord);
        db.MedicalRecord.hasMany(db.Treatment);

        db.Treatment.belongsTo(db.Calculations);
    }
}

};
