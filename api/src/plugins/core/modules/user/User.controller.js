
export function index(req, res, next) {
    req.db.User.findAll({
        attributes: ["id","email","username","firstname","lastname","profile_picture","gender","birthday","contact_no","employee_id","user_sub","status"],
        include:[req.db.PetShop, req.db.Role]
    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}


export function show(req, res, next) {
    req.db.User.find({
        where: {
            id: req.params.id
        },
        attributes: ["id","email","username","firstname","lastname","profile_picture","gender","birthday","contact_no","employee_id","user_sub","status"],
        include:[req.db.PetShop, req.db.Role]
    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}



export function create(req, res, next) {

    if (req.body.id) delete req.body.id;

    req.db.User.create(req.body)
        .then(req.handleResponse.respond(res, 201))
        .catch(req.handleResponse.handleError(res));

}


export function update(req, res, next) {

    req.db.User.find({
        where: {
            id: req.params.id
        },
        attributes: ["id","email","username","password","organization_client","firstname","lastname","facebook","profile_picture","gender","birthday","PetShopId","actions"],
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {

                obj.update(req.body, {
                    fields: ["id","email","username","password","organization_client","firstname","lastname","facebook","profile_picture","gender","birthday","PetShopId","actions"],
                })
                    .then(req.handleResponse.respond(res))
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));

}



export function destroy(req, res, next) {
    req.db.User.find({
        where: {
            id: req.params.id
        }
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {

                return obj
                    .destroy()
                    .then(() => {
                        return res.status(204).end();
                    })
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));
}
