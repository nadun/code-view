module.exports = function (sequelize, DataTypes) {
    var User = sequelize.define('User', {
        user_sub: DataTypes.STRING,
        email: DataTypes.STRING,
        contact_no: DataTypes.STRING,
        username: DataTypes.STRING,
        firstname: DataTypes.STRING,
        lastname: DataTypes.STRING,
        employee_id: DataTypes.STRING,
        profile_picture: DataTypes.STRING,
        gender: {
            type: DataTypes.ENUM,
                values: ["male","female"]
            },
        status: {
            type: DataTypes.ENUM,
                values: ["Active","Deactivated"]
            },
        birthday: DataTypes.DATE,
        role: DataTypes.STRING,
                
    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    return User;
}