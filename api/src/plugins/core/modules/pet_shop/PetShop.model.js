module.exports = function (sequelize, DataTypes) {
    var PetShop = sequelize.define('PetShop', {
        title: DataTypes.STRING,
        domain_name: { type: DataTypes.STRING, unique: true },
        email: { type: DataTypes.STRING, unique: true },
        phone_number: DataTypes.STRING,
        first_name: DataTypes.STRING,
        last_name: DataTypes.STRING,
        country: DataTypes.STRING,
        marketing: DataTypes.BOOLEAN, //Please don’t send me any marketing communications

    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    return PetShop;
}