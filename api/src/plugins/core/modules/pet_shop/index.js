let express = require('express');

let controller = require('./PetShop.controller');

let router = express.Router();

module.exports = function (app, db) {


    db.PetShop = db.sequelize.import('./PetShop.model');

    router.get('/', controller.index);
    router.get('/:id([0-9]+)', controller.show);
    router.post('/', controller.create);
    router.put('/:id([0-9]+)', controller.update);
    router.delete('/:id([0-9]+)', controller.destroy);
    router.post('/delete/', controller.deleteAll);
    router.get('/petshopsByEmail/:email', controller.petshopsByEmail);
    
    return {
        router: router,
        relateModels: function () {
            db.PetShop.hasMany(db.Pet);
            db.Pet.belongsTo(db.PetShop);

            db.PetShop.hasMany(db.Breed);
            db.Breed.belongsTo(db.PetShop);

            db.PetShop.hasMany(db.Breeder);
            db.Breeder.belongsTo(db.PetShop);

            db.PetShop.hasMany(db.Administered_type);
            db.Administered_type.belongsTo(db.PetShop);

            db.PetShop.hasMany(db.Calculations);
            db.Calculations.belongsTo(db.PetShop);

            db.PetShop.hasMany(db.Contract);
            db.Contract.belongsTo(db.PetShop);

            db.PetShop.hasMany(db.Customer);
            db.Customer.belongsTo(db.PetShop);

            db.PetShop.hasMany(db.MedicalRecord);
            db.MedicalRecord.belongsTo(db.PetShop);

            db.PetShop.hasMany(db.MedicalRecordVaccination);
            db.MedicalRecordVaccination.belongsTo(db.PetShop);

            db.PetShop.hasMany(db.Registration_type);
            db.Registration_type.belongsTo(db.PetShop);

            db.PetShop.hasMany(db.Treatment);
            db.Treatment.belongsTo(db.PetShop);

            db.PetShop.hasMany(db.Vaccination);
            db.Vaccination.belongsTo(db.PetShop);

            db.PetShop.hasMany(db.User);
            db.User.belongsTo(db.PetShop);
        }
    }

};
