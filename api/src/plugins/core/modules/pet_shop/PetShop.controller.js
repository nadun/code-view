
// import { destroy } from './../payment/Payment.controller';
import axios from "axios";
import { resolve } from "dns";
import { rejects } from "assert";
import _ from "lodash";

export function index(req, res, next) {
    req.db.PetShop.findAll({

    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}


export function show(req, res, next) {
    req.db.PetShop.find({
        where: {
            id: req.params.id
        },
        attributes: ["id", "email", "title", "first_name", "last_name", "country", "domain_name"]
    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}



export function create(req, res, next) {

    var data = {
        "type": "CNAME",
        "name": req.body.domain,
        "content": "127.0.0.1",
        "ttl": 120,
        "priority": 10,
        "proxied": false
    };
    //console.log(req.body.domain);
    req.db.PetShop.find({
        where: {
            domain_name: req.body.domain
        }
    })
        .then(response => {
            //console.log(response);
            if (_.isEmpty(response)) {
                axios.post(req.config.CF_baseUrl+'zones/'+req.config.CF_zone+'/dns_records', data, req.config.CF_headers)
                    .then(function (response) {
                        console.log(response);
                        if (req.body.id) delete req.body.id;
                        return req.db.PetShop.create({ domain_name: req.body.domain });
                    })
                    .catch(function (error) {

                        return Promise.reject({ err: "Sub domain creation failed." });
                    })
                    .then(req.handleResponse.respond(res, 201))
                    .catch(req.handleResponse.handleError(res));
            } else {
                return Promise.reject({ err: "Domain name already exist" })
            }
        })
        .catch(req.handleResponse.handleError(res));



}


export function update(req, res, next) {

    req.db.PetShop.find({
        where: {
            id: req.params.id
        },
        attributes: ["id", "email", "title", "first_name", "last_name", "country"],
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {

                obj.update(req.body, {
                    fields: ["id", "email", "title", "first_name", "last_name", "country"],
                })
                    .then(req.handleResponse.respond(res))
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));

}

export function destroy(req, res, next) {
    req.db.PetShop.find({
        where: {
            id: req.params.id
        }
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {

                return obj
                    .destroy()
                    .then(() => {
                        return res.status(204).end();
                    })
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));
}

export function deleteAll(req, res, next) {
    req.db.PetShop.destroy({
        where: {
            id: req.body.ids
        },
        individualHooks: true
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {
            return res.status(200).json(obj);
        })
        .catch(req.handleResponse.handleError(res));
}

export function petshopsByEmail(req, res, next) {
    req.db.PetShop.findAll({
        where: {
            email: req.params.email
        },
        attributes: ["id", "email", "title", "first_name", "last_name", "country"]
    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}