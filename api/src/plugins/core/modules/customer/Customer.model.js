module.exports = function (sequelize, DataTypes) {
    var Customer = sequelize.define('Customer', {
       name: DataTypes.STRING,
       address: DataTypes.STRING,
       city: DataTypes.STRING,
       state: DataTypes.STRING,  
       zip_code: DataTypes.STRING,
       phone_home: DataTypes.STRING,
       phone_cell: { type: DataTypes.STRING },
       email: { type: DataTypes.STRING },
       find_us: DataTypes.STRING,
    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    return Customer;
}