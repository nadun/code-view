import { isArray } from "util";
import async from "async";
import dateFormat from 'dateformat';

export function index(req, res, next) {
    req.db.Customer.findAll({
        
    })
        .then(req.handleResponse.respond(res))
        .catch(e=>{
            console.log(e);
            req.handleResponse.handleError(res)
        });
}

// export function getLeftMedicalRecords(req, res, next) {
//     req.db.MedicalRecord.findAll({
//         attributes: ["id", "arrival_date", "sex", "color_markings", "date_of_birth", "micro_chip", "cross_breed", "registration_type", "sire", "sire_number", "dam", "dam_number", "pet_id"],
//         include: [{model: req.db.Pet, attributes: ["id", "type", "default"]}, {model: req.db.Breed, attributes: ["id", "breed_type"]}, {model: req.db.Breeder, attributes: ["id", "breeder", "usda_no", "address", "phone"]}, {model: req.db.Registration_type, attribute: ["id", "type", "default"]}, {model: req.db.Treatment, required:false}],
//         where: { '$Treatments.MedicalRecordId$': {$eq: null} }
//     }).then(records => {

//         let recs = JSON.parse(JSON.stringify(records));
//         recs.forEach(rec => {
            
//             rec.pet_type = rec.Pet.type;
//             rec.breed_type = rec.Breed.breed_type;
//             rec.reg_type = rec.Registration_type.type;
//             rec.date_of_birth = dateFormat(rec.date_of_birth, "yyyy-mm-dd");
//             console.log(rec);
//         });

//         return recs;
            
//     })
//         .then(req.handleResponse.respond(res))
//         .catch(e=>{
//             console.log(e);
//             req.handleResponse.handleError(res)
//         });
// }


// export function show(req, res, next) {
//     req.db.MedicalRecord.find({
//         where: {
//             id: req.params.id
//         },
//         attributes: ["id", "description"],
//         include: [{model: req.db.Pet, attributes: ["id", "type", "default"]}, {model: req.db.Breed, attributes: ["id", "breed_type"]}, {model: req.db.Breeder, attributes: ["id", "breeder", "usda_no", "address", "phone"]}]
//     })
//         .then(req.handleResponse.respond(res))
//         .catch(req.handleResponse.handleError(res));
// }



// export function create(req, res, next) {

//     if (req.body.id) delete req.body.id;

//     // req.db.MedicalRecord.create(req.body)
//     // .then(req.handleResponse.respond(res, 201))
//     // .catch(req.handleResponse.handleError(res));

//     // return new Promise((resolve, reject) => {
//     //     if (req.body.registration_type_id) {
//     //         resolve(req.body.registration_type_id);
//     //     } else {
//     //         console.log("*************called 1 *********************");
//     //         req.db.Registration_type.create({
//     //             type: req.body.registration_type
//     //         })
//     //             .then(res => {
//     //                 resolve(res)
//     //             })
//     //             .catch(err => {
//     //                 console.log(err)
//     //                 reject(err)
//     //             })
//     //     }
//     // })
//     //     .then(responseRegistrationType => {
//     //         console.log("*************called 2 *********************");

//     //         if (req.body.breeder_id) {
//     //             return req.body.breeder_id;
//     //         } else {
//     //             return req.db.Breeder.create({
//     //                 breeder: req.body.breeder,
//     //                 usda_no: req.body.usda_no,
//     //                 address: req.body.breeder_address,
//     //                 phone: req.body.breeder_phone
//     //             })
//     //                 .then(responseFromBreeder => {
//     //                     return {
//     //                         registration_type_id: responseRegistrationType.id,
//     //                         breeder_id: responseFromBreeder.id
//     //                     }
//     //                 })
//     //         }
//     //     })
//         // .then(responseFromBreeder => {
//             console.log("*************called 3 *********************");
//             return new Promise((resolve, reject) => {
//                 let vaccinationsArr = [];
//                 if (isArray(req.body.vaccines)) {
//                     async.each(req.body.vaccines, function (vaccination, callback) {
//                         console.log("**vacc**", vaccination)
//                         vaccinationsArr.push({
//                             id: vaccination.administered,
//                             date: vaccination.date,
//                             more_info: vaccination.more_info
//                         });
//                         callback();
//                         // if (vaccination.id) {
//                         //     vaccinationsArr.push(resp.id);
//                         //     callback();
//                         // } else {
//                         //     req.db.Vaccination.create({
//                         //         name: vaccination.administered,
                                
//                         //     })
//                         //         .then(newVaccination => {

//                         //             let resp = newVaccination.get({
//                         //                 plain: true
//                         //             })

//                         //             vaccinationsArr.push({
//                         //                 id: resp.id,
//                         //                 date: vaccination.date,
//                         //                 more_info: vaccination.more_info
//                         //             });

//                         //             callback();
//                         //         })
//                         // }

//                     }, function (err) {
//                         if (err) {
//                             console.log('A file failed to process');
//                         } else {

//                             return resolve(vaccinationsArr);
//                         }
//                     });
//                 }
//             })

//         // })
//         .then(responseVaccinationsArr => {

//             console.log("*************called 4 *********************", responseVaccinationsArr, req.body);

//             req.body.arrival_date = req.body.arrival;
//             req.body.PetId = req.body.top;
//             req.body.BreedId = req.body.breed;
//             req.body.date_of_birth = req.body.dob;
//             req.body.RegistrationTypeId = req.body.regType;
//             // req.body.MedicalRecordVaccination = responseVaccinationsArr;
//             req.db.MedicalRecord.create(req.body)
//                 .then(res => {

//                     let resp = res.get({
//                         plain: true
//                     })
//                     let objArr = [];

//                     for (let va of responseVaccinationsArr) {
//                         objArr.push({ MedicalRecordId: resp.id, VaccinationId: va.id, date: va.date, more_info: va.more_info })
//                     }

//                     console.log("-----------aaa--------------", objArr);

//                     return req.db.MedicalRecordVaccination.bulkCreate(objArr)
//                         .then(res2 => {
//                             console.log("-----------sef--------------", res2);

//                             // let resp2 = res2.get({
//                             //     plain: true
//                             // })

//                             resp.vaccination = objArr;



//                             return resp;
//                         })
//                 })
//                 .then(req.handleResponse.respond(res, 201))
//                 .catch(req.handleResponse.handleError(res));

//         })

// }


// export function update(req, res, next) {

//     req.db.MedicalRecord.find({
//         where: {
//             id: req.params.id
//         }
//     })
//         .then(req.handleResponse.checkNotFound(res))
//         .then((obj) => {

//             if (obj) {

//                 obj.update(req.body, {
//                     fields: ["id", "description", "actions", "PetId", "PetStoreId", "PetId", "PetStoreId"],
//                 })
//                     .then(req.handleResponse.respond(res))
//                     .catch(req.handleResponse.handleError(res));
//             }
//         })
//         .catch(req.handleResponse.handleError(res));

// }



// export function destroy(req, res, next) {
//     req.db.MedicalRecord.find({
//         where: {
//             id: req.params.id
//         }
//     })
//         .then(req.handleResponse.checkNotFound(res))
//         .then((obj) => {

//             if (obj) {

//                 return obj
//                     .destroy()
//                     .then(() => {
//                         return res.status(204).end();
//                     })
//                     .catch(req.handleResponse.handleError(res));
//             }
//         })
//         .catch(req.handleResponse.handleError(res));
// }
