let express = require('express');

let controller = require('./Customer.controller');
        
let router = express.Router();

module.exports = function (app, db) {

    db.Customer = db.sequelize.import('./Customer.model');
        
    router.get('/', controller.index); 
    // router.get('/:id([0-9]+)', controller.show); 
    // router.post('/', controller.create); 
    // router.put('/:id([0-9]+)', controller.update); 
    // router.delete('/:id([0-9]+)', controller.destroy); 

    return { 
        router: router,
        relateModels: function(){
            
        }
    }

};
