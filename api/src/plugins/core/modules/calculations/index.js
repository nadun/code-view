let express = require('express');


let controller = require('./Calculations.controller');
        
let router = express.Router();

module.exports = function (app, db) {

    
db.Calculations = db.sequelize.import('./Calculations.model');
        
router.get('/', controller.index);
router.get('/:id([0-9]+)', controller.show); 
router.post('/', controller.create);
router.post('/delete/', controller.deleteAll);
router.get('/calculationsByType/:type', controller.calculationsByType)

return { 
    router: router,
    // relateModels: function(){
    //     db.Calculations.belongsTo(db.Treatment);
    // }
}

};
