module.exports = function (sequelize, DataTypes) {
    var Calculations = sequelize.define('Calculations', {
        doseMg: {
            type: DataTypes.STRING
        },
        doseLb: {
            type: DataTypes.STRING
        },
        dosageLb: {
            type: DataTypes.STRING
        },
        doseTimes: {
            type: DataTypes.STRING
        },
        duration: {
            type: DataTypes.STRING
        },
        strengthMg: {
            type: DataTypes.STRING
        },
        strengthMl: {
            type: DataTypes.STRING
        },
        treatmentName: {
            type: DataTypes.STRING
        },
        calculationType: {
            type: DataTypes.STRING
        },
        dosageMl: {
            type: DataTypes.STRING
        }
        
    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    return Calculations;
}