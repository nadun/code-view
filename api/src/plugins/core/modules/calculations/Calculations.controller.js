
export function index(req, res, next) {
    req.db.Calculations.findAll({
        
    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}

export function show(req, res, next) {
    req.db.Calculations.find({
        where: {
            id: req.params.id
        }
    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}

export function create(req, res, next) {

    if (req.body.id) delete req.body.id;

    req.db.Calculations.create(req.body)
        .then(req.handleResponse.respond(res, 201))
        .catch(req.handleResponse.handleError(res));

}

export function deleteAll(req, res, next) {
    req.db.Calculations.destroy({
        where: {
            id: req.body.ids
        },
        individualHooks: true
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {
            return res.status(200).json(obj);
        })
        .catch(req.handleResponse.handleError(res));
}

export function calculationsByType(req, res) {
    req.db.Calculations.findAll({
        where: {
            calculationType: req.params.type
        }
    })
    .then(req.handleResponse.respond(res))
    .catch(req.handleResponse.handleError(res));
}
