
export function index(req, res, next) {
    req.db.Breed.findAll({
        // attributes: ["id","breed_type","actions","PetId"],
        include: [req.db.Pet]

    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}


export function show(req, res, next) {
    console.log("here in th show ");
    req.db.Breed.find({
        where: {
            id: req.params.id
        },
        attributes: ["id", "breed_type", "actions"],

    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}



export function create(req, res, next) {
    console.log('req.bbody', req.body);
    if (req.body.id) delete req.body.id;

    req.db.Breed.create(req.body)
        .then(req.handleResponse.respond(res, 201))
        .catch(req.handleResponse.handleError(res));

}


export function update(req, res, next) {
    console.log("the request is ############## ", req.body);
    req.db.Breed.find({
        where: {
            id: req.params.id
        },
        attributes: ["id", "breed_type", "actions", "PetId"],
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {

                obj.update(req.body, {
                    fields: ["id", "breed_type", "actions", "PetId"],
                })
                    .then(req.handleResponse.respond(res))
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));

}



export function destroy(req, res, next) {
    req.db.Breed.find({
        where: {
            id: req.params.id
        }
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {

                return obj
                    .destroy()
                    .then(() => {
                        return res.status(204).end();
                    })
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));
}

export function deleteAll(req, res, next) {
    console.log ('req.body.ids ##################################',req.body.ids );
    req.db.Breed.destroy({
        where: {
            id: req.body.ids
        },
        
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {
            return res.status(200).json(obj);
        })
        .catch(req.handleResponse.handleError(res));
}

export function getBreedsByAnimal(req, res, next) {
    console.log("here breeds ", req.params);
    req.db.Breed.findAll({
        where: {
            petId: req.params.id
        },
        attributes: ["id", "breed_type", "actions"],

    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}
