module.exports = function (sequelize, DataTypes) {
    var Breed = sequelize.define('Breed', {
        breed_type: DataTypes.STRING,
        actions: DataTypes.STRING,
                
    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    return Breed;
}