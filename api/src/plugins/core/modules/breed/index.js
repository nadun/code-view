let express = require('express');

let controller = require('./Breed.controller');

let router = express.Router();

module.exports = function (app, db) {

    db.Breed = db.sequelize.import('./Breed.model');

    router.get('/', controller.index);
    router.get('/:id([0-9]+)', controller.show);
    router.get('/getBreedsByAnimal/:id([0-9]+)', controller.getBreedsByAnimal);
    router.post('/', controller.create);
    router.put('/:id([0-9]+)', controller.update);
    router.delete('/:id([0-9]+)', controller.destroy);
    router.post('/delete/', controller.deleteAll);


    return {
        router: router,
        relateModels: function () {
            db.Breed.belongsTo(db.Pet);
        }
    }

};
