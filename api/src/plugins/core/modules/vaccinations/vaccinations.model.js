module.exports = function (sequelize, DataTypes) {
    var Vaccination = sequelize.define('Vaccination', {
        name: DataTypes.STRING,
    },
        {
            timestamps: true,
            paranoid: true
        }
    );

    return Vaccination;
}