
export function index(req, res, next) {
    req.db.Vaccination.findAll({
        attributes: ["id","name"],
        
    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}


export function show(req, res, next) {
    req.db.Vaccination.find({
        where: {
            id: req.params.id
        },
        attributes: ["id","name"],
        
    })
        .then(req.handleResponse.respond(res))
        .catch(req.handleResponse.handleError(res));
}



export function create(req, res, next) {

    if (req.body.id) delete req.body.id;

    req.db.Vaccination.create(req.body)
        .then(req.handleResponse.respond(res, 201))
        .catch(req.handleResponse.handleError(res));

}


export function update(req, res, next) {

    req.db.Vaccination.find({
        where: {
            id: req.params.id
        },
        attributes: ["id","name"],
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {

                obj.update(req.body, {
                    fields: ["id","name"],
                })
                    .then(req.handleResponse.respond(res))
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));

}



export function destroy(req, res, next) {
    req.db.Vaccination.find({
        where: {
            id: req.params.id
        }
    })
        .then(req.handleResponse.checkNotFound(res))
        .then((obj) => {

            if (obj) {

                return obj
                    .destroy()
                    .then(() => {
                        return res.status(204).end();
                    })
                    .catch(req.handleResponse.handleError(res));
            }
        })
        .catch(req.handleResponse.handleError(res));
}
