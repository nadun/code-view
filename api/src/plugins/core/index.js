
var plugin = {
    name: 'core',
    modules: ["user","role","userrole","pet","breed","payment","medicalrecord","treatment","breeder", "registration_type", "vaccinations", "administered_type", "calculations", "contract", "customer", "pet_shop"]

}

module.exports = plugin;
