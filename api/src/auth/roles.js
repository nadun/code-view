var db = require('../lib/db');
var _ = require('lodash');

export default {
    getUserRoles: function(congnito_id){
        return db.UserRole.findAll({
            include: [{
                model: db.Role,
                attributes: ['role_name']
            },{
                model: db.User,
                where: {congnito_id: congnito_id},
                attributes: []
            }],
            attributes: []
        })
        .then(response => {
            return response;
        })
    },
    hasRole: function(role){

        return function(req, res, next){

            let congnito_id = res.locals.user.sub;

            if(_.isArray(role)){

                let rolesSet = [];
                
                role.forEach(value => {
                    rolesSet.push(
                        {role_name: role}
                    )
                })
                
                db.UserRole.findAll({
                    include: [{
                        model: db.Role,
                        attributes: ['role_name'],
                        where: {
                            $or: rolesSet
                        }
    
                    },{
                        model: db.User,
                        where: {congnito_id: congnito_id},
                        attributes: []
                    }],
                    attributes: []
                })
                .then(response => {
                    let data = JSON.parse(JSON.stringify(response));
    
                    if(_.isEmpty(data)){
                        return res.status(401).send("unauthorized access.");
                    }else{
                        next();
                    }
                })
            }else if(role){
                return db.UserRole.findAll({
                    include: [{
                        model: db.Role,
                        attributes: ['role_name'],
                        where: {role_name: role},
    
                    },{
                        model: db.User,
                        where: {congnito_id: congnito_id},
                        attributes: []
                    }],
                    attributes: []
                })
                .then(response => {
                    return response;
                })
            }else{
                return false;
            }
        }



    }
}