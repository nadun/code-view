
var Sequelize = require('sequelize');
var config = require('../config');

module.exports = {
    Sequelize,
    sequelize: new Sequelize(config.sequelize.uri, config.sequelize.options)
};
