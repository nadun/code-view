export default {
    respond: function (res, status = 200) {
        return function (data) {
            if (data == null) {
                return res.status(404).json({ status: 404, message: 'Not Found!' });
            }
            return res.status(status).json(data);
        }
    },
    checkNotFound: function (res) {
        return function (data) {
            
            if (data == null) {
                res.status(404).json({ status: 404, message: 'Not Found!' });
            }
            return data;
        }
    },
    handleError: function (res) {
        return function (err) {

            if (err.name) {

                switch (err.name) {
                    case 'SequelizeUniqueConstraintError':
                        return res.status(400).json({ status: 'ERROR', message: err.message });
                    default:
                        return res.status(400).json({ status: 'ERROR', details: err });
                }

            }
            console.log(err);
            return res.status(400).json({ status: 'ERROR', details: err });
        }
    }
}