import fs from 'fs';
import path from 'path';


import enabledPlugins from '../plugins';//get enabled plugings

var modelRelations = [];

function moduleLoader(plugin, app, db) {
    let pluginInfo = require(plugin);

    console.log(`${pluginInfo.name} plugin loaded.`);

    for (let load_module of pluginInfo.modules) {//loop through enabled modules

        let p = path.normalize(plugin + '/modules/' + load_module); //path for the plugin module

        if (fs.existsSync(p)) {//if path exists
            //moduleLoader(p, app);//load modules of it
            console.log('injecting route bundle - /' + pluginInfo.name + '/' + load_module);

            //push to route stack

            let mod = require(p)(app, db);
            modelRelations.push(mod.relateModels);

            app.use('/' + pluginInfo.name + '/' + load_module, mod.router);



        } else {
            console.log(`${load_module} module in ${pluginInfo.name} plugin not found on path - ${p}`);
        }

    }

}

export default function pluginLoader(app) {//plugin loader function

    var db = require('./db');

    app.use(function (req, res, next) {
        req.db = db;
        next();
    });

    app.set('db', db);


    for (let plugin of enabledPlugins) {//loop through enabled plugins

        let p = path.normalize(path.dirname(__filename) + '/../plugins/' + plugin); //path for the plugin

        if (fs.existsSync(p)) {//if path exists
            moduleLoader(p, app, db);//load modules of it
        } else {
            console.log(`${plugin} plugin not found on path - ${p}`);
        }

    }

    // let models = [];

    for (let fn of modelRelations) {//relate models
        if(fn){            
            fn();
        }
    }


    db.sequelize.sync({force: false})
        .then(function () {
            console.log("DB Sync Done");

        })
        .catch(function (err) {
            console.log('Server failed to start due to error: %s', err);
        });



}